// checkUserExists
const checkUserExists = async (req, reply) => {
  const connection = await global.fastify.mysql.getConnection()
  const [rows] = await connection.query(
    'SELECT * FROM `users` where `contact` = ?', [req.params.contact]
  )
  connection.release()
  console.log(rows[0].first_name)

  const data = { contact: req.params.contact }
  console.log(data)
  return data
}

const getUserList = async (req, reply) => {
  const connection = await global.fastify.mysql.getConnection()
  const [rows] = await connection.query(
    'SELECT * FROM users'
  )
  connection.release()
  return rows[0]
}

module.exports = {
  checkUserExists,
  getUserList
}
