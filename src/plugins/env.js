const fp = require('fastify-plugin')

const schema = {
  type: 'object',
  required: ['PORT'],
  properties: {
    PORT: {
      type: 'string',
      default: 3000
    },
    HOST: {
      type: 'string',
      default: '0.0.0.0'
    },
    DB_HOST: {
      type: 'string',
      default: ''
    },
    DB_USER: {
      type: 'string',
      default: ''
    },
    DB_PASS: {
      type: 'string',
      default: ''
    },
    DB_NAME: {
      type: 'string',
      default: ''
    },
    JWT_ACCESS_TOKEN: {
      type: 'string',
      default: ''
    },
    SWAGGER_HOST: {
      type: 'string',
      default: ''
    },
    TW_ACCOUNT_SID: {
      type: 'string',
      default: ''
    },
    TW_AUTH_TOKEN: {
      type: 'string',
      default: ''
    },
    TW_TWILIO_NUMBER: {
      type: 'string',
      default: ''
    },
    F_SERVER_KEY: {
      type: 'string',
      default: ''
    },
  }
}

const envOptions = {
  dotenv: true, // will read .env in root folder
  schema: schema
  // confKey: 'config' // optional, default: 'config'
  // data: data // optional, default: process.env
}

module.exports = fp(function (fastify, options, next) {
  fastify.register(require('fastify-env'), envOptions).ready((err) => {
    if (err) console.error(err)

    // console.log(fastify.config) // or fastify[options.confKey]
    // output: { PORT: 3000 }
  })

  next()
})
