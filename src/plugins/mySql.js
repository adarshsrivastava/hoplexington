const fp = require('fastify-plugin')

module.exports = fp(function (fastify, options, next) {
  const dbHost = fastify.config.DB_HOST
  const dbUser = fastify.config.DB_USER
  const dbPass = fastify.config.DB_PASS
  const dbName = fastify.config.DB_NAME

  fastify.register(require('fastify-mysql'), {
    promise: true,
    connectionString: 'mysql://' + dbUser + ':' + dbPass + '@' + dbHost + '/' + dbName
  })

  next()
})
