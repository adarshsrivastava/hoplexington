const fp = require("fastify-plugin");

module.exports = fp(function (fastify, options, next) {
  fastify.register(require("fastify-swagger"), {
    routePrefix: "/api-docs",
    swagger: {
      info: {
        title: "HopLexington APIs",
        description:
          "HopLexington APIs are listed here. And they are also grouped for find them easily. Thank you.",
        version: "1.0.0",
      },
      externalDocs: {
        url: "https://swagger.io",
        description: "Find more info here",
      },
      hosts: [fastify.config.SWAGGER_HOST],
      schemes: ["http", "https"],
      consumes: ["application/json"],
      produces: ["application/json"],
    },
    uiConfig: {
      docExpansion: "list", // full, list, none
      deepLinking: false,
    },
    staticCSP: false,
    transformStaticCSP: (header) => header,
    exposeRoute: true,
  });

  next();
});
