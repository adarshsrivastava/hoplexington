const fp = require('fastify-plugin')

module.exports = fp(async function (fastify, options, next) {
  fastify.register(require('fastify-jwt'), {
    secret: fastify.config.JWT_ACCESS_TOKEN
  })

  fastify.decorate('authenticate', async function (request, reply) {
    try {
      await request.jwtVerify()
    } catch (err) {
      reply.send(err)
    }
  })

  next()
})
