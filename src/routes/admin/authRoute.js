const AdminController = require("../../controllers/admin/AdminController");
const schemas = require("../../schema/admin/adminSchema");

const routes = async (fastify, options, next) => {
  fastify.get(
    "/send-otp/:contact",
    { schema: schemas.sendOtp },
    AdminController.sendOtp
  );
  fastify.get(
    "/send-email-otp/:contact",
    { schema: schemas.sendOtp },
    AdminController.sendEmailOtp
  );

  fastify.post("/login", { schema: schemas.login }, AdminController.login);
  fastify.post(
    "/venue-login",
    { schema: schemas.venue_login },
    AdminController.venue_login
  );

  fastify.get(
    "/is-login",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.isLogin,
    },
    AdminController.isLogin
  );

  next();
};

module.exports = routes;
