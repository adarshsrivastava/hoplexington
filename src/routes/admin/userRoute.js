const AdminController = require("../../controllers/admin/AdminController");
const schemas = require("../../schema/admin/adminSchema");

const routes = async (fastify, options, next) => {
  fastify.get(
    "/me",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.users,
    },
    AdminController.me
  );

  fastify.get(
    "/users",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.users,
    },
    AdminController.users
  );

  fastify.get(
    "/user/:id",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.userDetails,
    },
    AdminController.userDetails
  );

  // Add paid user
  fastify.post(
    "/create-new-paid-user",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.createNewPaidUser,
    },
    AdminController.createNewPaidUser
  );

  // get paid user
  fastify.get(
    "/get-all-paid-user-added-by-admin",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.selectPaidUserViaUser,
    },
    AdminController.selectPaidUserViaUser
  );

  // Add paid user
  fastify.post(
    "/delete-paid-user",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.deleteNewPaidUser,
    },
    AdminController.deleteNewPaidUser
  );

  // get-users-count
  fastify.get(
    "/get-users-count",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.getUsersCount,
    },
    AdminController.getUsersCount
  );

  // update-ref-code
  fastify.post(
    "/update-ref-code",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.updateRefCode,
    },
    AdminController.updateRefCode
  );

  // get-code
  fastify.get(
    "/get-referrals",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.getReferrals,
    },
    AdminController.getReferrals
  );

  fastify.post(
    "/mark-as-paid",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.markAsPaid,
    },
    AdminController.markAsPaid
  );

  next();
};

module.exports = routes;
