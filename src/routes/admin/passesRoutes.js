const AdminController = require("../../controllers/admin/AdminController");
const schemas = require("../../schema/admin/adminSchema");

const routes = async (fastify, options, next) => {
  fastify.get(
    "/passes",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.getPasses,
    },
    AdminController.getPasses
  );

  fastify.post(
    "/pass/add-new",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.addPass,
    },
    AdminController.addPass
  );

  fastify.post(
    "/pass/update/:id",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.updatePass,
    },
    AdminController.updatePass
  );

  fastify.get(
    "/get-media",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.getMedia,
    },
    AdminController.getMedia
  );

  fastify.post(
    "/remove-media/:id",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.removeMedia,
    },
    AdminController.removeMedia
  );

  fastify.post(
    "/send-push-notification",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.sendPushNotification,
    },
    AdminController.sendPushNotification
  );

  fastify.post(
    "/schedule-push-notification",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.schedulePushNotification,
    },
    AdminController.schedulePushNotification
  );

  fastify.get(
    "/get-scheduled-push-notification",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.getSchedulePushNotification,
    },
    AdminController.getSchedulePushNotification
  );

  fastify.post(
    "/delete-scheduled-push-notification",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.deleteSchedulePushNotification,
    },
    AdminController.deleteSchedulePushNotification
  );

  fastify.get(
    "/get-swipes-count",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.getSwipesCount,
    },
    AdminController.getSwipesCount
  );

  fastify.get(
    "/get-recurring-swipes-count",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.getSwipesCount,
    },
    AdminController.getRecurringSwipesCount
  );

  fastify.get(
    "/get-swipes-count-per-pass",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.getSwipesCount,
    },
    AdminController.getSwipesCountPerPass
  );

  next();
};

module.exports = routes;
