const AdminController = require("../../controllers/admin/AdminController");
const schemas = require("../../schema/admin/adminSchema");

const routes = async (fastify, options, next) => {
  fastify.get(
    "/locations",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.all_locations,
    },
    AdminController.allLocations
  );
  fastify.get(
    "/bars",
    {
      preValidation: [fastify.authenticate, fastify.isAdmin],
      schema: schemas.all_bars,
    },
    AdminController.allBars
  );

  next();
};

module.exports = routes;
