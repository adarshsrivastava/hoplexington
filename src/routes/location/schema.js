const response_mediums = require("../../schema/common/response");
const header_mediums = require("../../schema/common/header");

const location_schema = {
    type: 'object',
    properties: {
        name: { type: 'string', example: 'Location Name' }
    },
    required: ['name']
};
const location_schema_update = {
    type: 'object',
    properties: {
        location_id: { type: 'number', example: 1 }
    },
    required: ['location_id']
};

module.exports = {
    create: {
        description: 'Create a Location',
        tags: ['Location :: Admin'],
        summary: 'Create a Location',
        body: location_schema,
        headers: header_mediums,
        response: response_mediums
    },
    update: {
        description: 'Update Location',
        tags: ['Location :: Admin'],
        summary: 'Update Location Data',
        params: {
            id: { type: 'number' }
        },
        body: location_schema,
        headers: header_mediums,
        response: response_mediums
    },
    user_location_id: {
        description: 'Update Location for User',
        tags: ['Location :: Client'],
        summary: 'Bind Location Data with User',
        body: location_schema_update,
        headers: header_mediums,
        response: response_mediums
    },
    retrieve: {
        description: 'Get All Location details',
        tags: ['Location :: Client'],
        summary: 'Get All Locations',
        headers: header_mediums,
        response: response_mediums
    },
    retrieve_by_id: {
        description: 'Get a single Location details',
        tags: ['Location :: Client'],
        summary: 'Get All Bars inside a Location',
        params: {
            id: { type: 'number' }
        },
        headers: header_mediums,
        response: response_mediums
    }
};