const LocationController = require("../../controllers/locations/LocationController");
const schema = require("./schema");

const routes = async (fastify, options, next) => {
  // preValidation: [fastify.authenticate, fastify.isAdmin],
  fastify.post("/location/add-location", {
    onRequest: [fastify.authenticate],
    schema: schema.create,
    handler: LocationController.createLocation,
  });

  fastify.put("/location/update/:id", {
    onRequest: [fastify.authenticate],
    schema: schema.update,
    handler: LocationController.updateLocation,
  });

  fastify.get("/locations", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve,
    handler: LocationController.getLocations,
  });

  fastify.get("/locations/:id", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve_by_id,
    handler: LocationController.getLocationById,
  });
  fastify.get("/locations/:id/bars", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve_by_id,
    handler: LocationController.getBarsOnLocationById,
  });

  fastify.get("/bar/:id/passes", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve_by_id,
    handler: LocationController.getPassesOnBarsById,
  });
  fastify.get("/location/:id/passes", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve_by_id,
    handler: LocationController.getPassesOnLocationById,
  });

  fastify.put("/user/location", {
    onRequest: [fastify.authenticate],
    schema: schema.user_location_id,
    handler: LocationController.updateLocationForUser,
  });

};

module.exports = routes;
