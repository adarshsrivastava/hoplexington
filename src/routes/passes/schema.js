const response_mediums = require("../../schema/common/response");
const header_mediums = require("../../schema/common/header");

const create_token_schema = {
  type: "object",
  properties: {
    pass_id: { type: "number", example: 1 },
    quantity: { type: "number", example: 1 },
    charge_route: { type: "string", example: "stripe" },
  },
  required: ["pass_id", "quantity", "charge_route"],
};

const charge_schema = {
  type: "object",
  properties: {
    transaction_id: { type: "string", example: "ch_xxxxxxx|apple_key" },
    charge_route: { type: "string", example: "apple|stripe" },
  },
  required: ["transaction_id", "charge_route"],
};
const passes_schema = {
  type: "object",
  properties: {
    pass_id: { type: "number", example: 1 },
  },
  required: ["pass_id"],
};
const get_passes_schema = {
  type: "object",
  properties: {
    location_id: {
      type: "number",
      example: 7,
    },
  },
};

module.exports = {
  create: {
    description: "Create a Passes",
    tags: ["Passes :: My Passes"],
    summary: "Create a Favourite Pass",
    body: passes_schema,
    headers: header_mediums,
    response: response_mediums,
  },
  retrieve: {
    description: "Get All Passes details",
    tags: ["Passes :: My Passes"],
    summary: "Get All My Passess for user",
    headers: header_mediums,
    response: response_mediums,
  },
  retrieve_by_id: {
    description: "Get a single Passes details",
    tags: ["Passes :: My Passes"],
    summary: "Get All Bars inside a Passes",
    params: {
      id: { type: "number" },
    },
    headers: header_mediums,
    response: response_mediums,
  },
  delete_by_id: {
    description: "delete a single Passes details",
    tags: ["Passes :: My Passes"],
    summary: "delete a pass with id",
    params: {
      id: { type: "number" },
    },
    headers: header_mediums,
    response: response_mediums,
  },
  create_token: {
    description: "Create Token for Pass",
    tags: ["Stripe :: Charge"],
    summary: "Create Token for Pass",
    body: create_token_schema,
    headers: header_mediums,
    response: response_mediums,
  },

  capture_charge: {
    description: "Capture charge for Pass",
    tags: ["Stripe :: Charge"],
    summary: "Capture charge for Pass",
    body: charge_schema,
    headers: header_mediums,
    response: response_mediums,
  },
  retrieve_passes: {
    description: "Get All Passes details",
    tags: ["Passes :: Get Passes"],
    summary: "Get All My Passess for a location Id",
    params: {
      pass_type: {
        type: "string",
        example: "daily",
        enum: ["all", "daily", "upcoming", "line_hop"],
        description: "Passes can be {daily-upcoming-linehop}",
      },
    },
    query: get_passes_schema,
    headers: header_mediums,
    response: response_mediums,
  },
};
