const passController = require("../../controllers/passes/PassesController");
const myPassController = require("../../controllers/passes/MyPassesController");
const schemas = require("../../schema/passes/passesSchemas");

const MainPassController = require("../../controllers/passes/Passes");

const my_passes_schema = require("./schema");

const routes = (fastify, options, next) => {
  fastify.get(
    "/get-passes",
    { preValidation: [fastify.authenticate], schema: schemas.getPasses },
    passController.getPasses
  );

  fastify.get(
    "/get-details/:id",
    { preValidation: [fastify.authenticate], schema: schemas.getPassDetails },
    passController.getPassDetails
  );

  // My Passes Route

  fastify.post("/add-my-passes", {
    onRequest: [fastify.authenticate],
    schema: my_passes_schema.create,
    handler: myPassController.addToMyPasses,
  });

  fastify.get("/my-passes", {
    onRequest: [fastify.authenticate],
    schema: my_passes_schema.retrieve,
    handler: myPassController.getMyPassesOfUser,
  });

  // stripe charge for pass
  fastify.post("/create-token", {
    onRequest: [fastify.authenticate],
    schema: my_passes_schema.create_token,
    handler: myPassController.create_token,
  });
  fastify.post("/charge", {
    onRequest: [fastify.authenticate],
    schema: my_passes_schema.capture_charge,
    handler: myPassController.captureCharge,
  });

  fastify.get("/:pass_type", {
    onRequest: [fastify.authenticate],
    schema: my_passes_schema.retrieve_passes,
    handler: MainPassController.getPasses,
  });

  fastify.delete("/delete/pass/:id", {
    onRequest: [fastify.authenticate],
    schema: my_passes_schema.delete_by_id,
    handler: MainPassController.deletePass,
  });

  next();
};

module.exports = routes;
