const response_mediums = require("../../schema/common/response");
const header_mediums = require("../../schema/common/header");

const Bar_schema = {
    type: 'object',
    properties: {
        name: { type: 'string', example: 'Bar Name' },
        address_line_1: { type: 'string', example: 'address_line_1' },
        address_line_2: { type: 'string', example: 'address_line_2 ' },
        city: { type: 'string', example: 'city ' },
        state: { type: 'string', example: 'state ' },
        pin_code: { type: 'number', example: '1001 ' },
        latitude: { type: 'number', example: 23.5 },
        longitude: { type: 'number', example: 67.9 },
        banner: { type: 'string', example: 'banner.com/abc.jpg' },
        active: { type: 'boolean', example: true },
        location_id: { type: 'number', example: 1 },
    },
    required: ['name','address_line_1','address_line_2','city','state','pin_code','latitude','longitude','banner','active','location_id']
};
const Bar_schema_update = {
    type: 'object',
    properties: {
        bar_id: { type: 'number', example: 1 }
    },
    required: ['bar_id']
};

module.exports = {
    create: {
        description: 'Create a Bar',
        tags: ['Bar :: Admin'],
        summary: 'Create a Bar',
        body: Bar_schema,
        headers: header_mediums,
        response: response_mediums
    },
    update: {
        description: 'Update Bar',
        tags: ['Bar :: Admin'],
        summary: 'Update Bar Data',
        params: {
            id: { type: 'number' }
        },
        body: Bar_schema,
        headers: header_mediums,
        response: response_mediums
    },
    user_Bar_id: {
        description: 'Update Bar for User',
        tags: ['Bar :: Client'],
        summary: 'Bind Bar Data with User',
        body: Bar_schema_update,
        headers: header_mediums,
        response: response_mediums
    },
    retrieve: {
        description: 'Get All Bar details',
        tags: ['Bar :: Client'],
        summary: 'Get All Bars',
        headers: header_mediums,
        response: response_mediums
    },
    retrieve_by_id: {
        description: 'Get a single Bar details',
        tags: ['Bar :: Client'],
        summary: 'Get All Bars inside a Bar',
        params: {
            id: { type: 'number' }
        },
        headers: header_mediums,
        response: response_mediums
    }
};