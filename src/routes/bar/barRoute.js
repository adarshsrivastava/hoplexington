const BarController = require("../../controllers/bar/BarController");
const schema = require("./schema");

const routes = async (fastify, options, next) => {
  // preValidation: [fastify.authenticate, fastify.isAdmin],
  fastify.post("/bar/add-bar", {
    onRequest: [fastify.authenticate],
    schema: schema.create,
    handler: BarController.createBar,
  });

  fastify.put("/bar/update/:id", {
    onRequest: [fastify.authenticate],
    schema: schema.update,
    handler: BarController.updateBar,
  });

  fastify.get("/bars", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve,
    handler: BarController.getBars,
  });
  fastify.get("/bar/:id", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve_by_id,
    handler: BarController.getBarById,
  });
};

module.exports = routes;
