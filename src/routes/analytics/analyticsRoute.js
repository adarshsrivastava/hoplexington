const AnalyticsController = require("../../controllers/analytics/AnalyticsController");
const schema = require("./schema");

const routes = async (fastify, options, next) => {
  // preValidation: [fastify.authenticate, fastify.isAdmin],
  fastify.get("/get-metrics", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve,
    handler: AnalyticsController.getAnalytics,
  });
  fastify.get("/get-metrics/:id", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve_by_id,
    handler: AnalyticsController.getAnalyticsById,
  });
  fastify.get("/get-metrics-location/:id", {
    onRequest: [fastify.authenticate],
    schema: schema.retrieve_by_id,
    handler: AnalyticsController.getAnalyticsByLocationId,
  });
};

module.exports = routes;
