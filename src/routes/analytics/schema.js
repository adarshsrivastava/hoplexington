const response_mediums = require("../../schema/common/response");
const header_mediums = require("../../schema/common/header");

module.exports = {
  retrieve: {
    description: "Get All Location details",
    tags: ["Analytics"],
    summary: "Get All Metrics for Analytics",
    headers: header_mediums,
    response: response_mediums,
  },
  retrieve_by_id: {
    description: "Get a single Venue Analytics",
    tags: ["Analytics"],
    summary: "Get All Analytics for a Venue",
    params: {
      id: { type: "number" },
    },
    headers: header_mediums,
    response: response_mediums,
  },
};
