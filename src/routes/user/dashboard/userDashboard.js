const userDashboardController = require("../../../controllers/dashboard/userDashboardController");
const schemas = require("../../../schema/dashboard/userDashboardSchemas");

function routes(fastify, options, done) {
  // Get user dashboard
  fastify.get(
    "/user",
    { preValidation: [fastify.authenticate], schema: schemas.profile },
    userDashboardController.profile
  );

  // Get Passes redeemed by user
  fastify.get(
    "/redeemed-passes",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.getRedeemedPasses,
    },
    userDashboardController.getRedeemedPasses
  );

  // Redeem a new pass
  fastify.post(
    "/redeem-pass",
    { preValidation: [fastify.authenticate], schema: schemas.redeemPass },
    userDashboardController.redeemPass
  );

  // Update push notification status
  fastify.post(
    "/update-notification-status",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.updatePushNotification,
    },
    userDashboardController.updatePushNotification
  );

  // Rate a pass
  fastify.post(
    "/rate-a-pass",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.ratePass,
    },
    userDashboardController.ratePass
  );

  // Add or update an FCM Token
  fastify.post(
    "/update-fcm-token",
    { preValidation: [fastify.authenticate], schema: schemas.updateFcmToken },
    userDashboardController.updateFcmToken
  );

  // Remove an FCM Token
  fastify.post(
    "/remove-fcm-token",
    { preValidation: [fastify.authenticate], schema: schemas.removeFcmToken },
    userDashboardController.removeFcmToken
  );

  // check if paid user
  fastify.post(
    "/get-status-if-paid-via-admin",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.checkIfPaidViaAdmin,
    },
    userDashboardController.checkIfPaidViaAdmin
  );

  // check if paid user
  fastify.post(
    "/update-user-paid-status",
    {
      preValidation: [fastify.authenticate],
      schema: schemas.updatePaidStatus,
    },
    userDashboardController.updatePaidStatus
  );

  done();
}

module.exports = routes;
