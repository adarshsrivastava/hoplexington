const userController = require("../../controllers/UserController");
const schemas = require("../../schema/userSchemas");
const services = require("../../services/pushNotification");

function routes(fastify, options, done) {
  // Check user is exists
  fastify.get(
    "/check-user-exists/:contact",
    { schema: schemas.checkUserExists },
    userController.checkUserExists
  );

  // User login
  fastify.post(
    "/login",
    { schema: schemas.userLogin },
    userController.userLogin
  );

  // Register a new user
  fastify.post(
    "/register",
    { schema: schemas.userRegister },
    userController.userRegister
  );

  // Register a new user
  fastify.post(
    "/verify-new-user-otp",
    { schema: schemas.verifyNewUserOtp },
    userController.verifyNewUserOtp
  );

  // Register a new user
  fastify.post(
    "/verify-referral",
    { schema: schemas.verifyReferral },
    userController.verifyReferral
  );

  // Remove an existing user
  fastify.delete(
    "/rm-user/:contact",
    { schema: schemas.rmUser },
    userController.rmUser
  );

  done();
}

module.exports = routes;
