const tzString = "America/New_York";

const twoDigits = (d) => {
  if (d >= 0 && d < 10) return "0" + d.toString();
  if (d > -10 && d < 0) return "-0" + (-1 * d).toString();
  return d.toString();
};

const changeZone = (date) => {
  const localTime = new Date(
    date.toLocaleString("en-US", { timeZone: tzString })
  );
  // console.log(`==================CURRENT TIME: ${localTime}=====================`);
  return localTime;
};

const getDateFormatted = (cd) => {
  MyDate = changeZone(cd);
  return (
    MyDate.getFullYear() +
    "-" +
    twoDigits(1 + MyDate.getMonth()) +
    "-" +
    twoDigits(MyDate.getDate()) +
    " " +
    twoDigits(MyDate.getHours()) +
    ":" +
    twoDigits(MyDate.getMinutes()) +
    ":" +
    twoDigits(MyDate.getSeconds())
  );
};

const getDateMinuteFormatted = (cd) => {
  MyDate = changeZone(cd);
  return (
    MyDate.getFullYear() +
    "-" +
    twoDigits(1 + MyDate.getMonth()) +
    "-" +
    twoDigits(MyDate.getDate()) +
    " " +
    twoDigits(MyDate.getHours()) +
    ":" +
    twoDigits(MyDate.getMinutes()) +
    ":00"
  );
};

const getDayFormatted = (cd) => {
  MyDate = changeZone(cd);
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  const d = new Date(MyDate);
  const dayName = days[d.getDay()];
  return dayName;
};

const getDateOfDay = (d) => {
  const currentDate = changeZone(new Date());
  const days = [
    "Sunday",
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
  ];
  let index = currentDate.getDay();
  if (days[index] == d) {
    let nextDateToReturn = new Date();
    nextDateToReturn.setMinutes(nextDateToReturn.getMinutes() + 1);
    nextDateToReturn = getDateFormatted(nextDateToReturn);
    return nextDateToReturn;
  } else {
    let count = 0;
    let nextDate = 0;
    for (i = 0; i < 7; i++) {
      index++;
      count++;
      if (index > 6) {
        index = 0;
      }
      if (days[index] == d) {
        nextDate = count;
      }
    }
    let nextDateToReturn = new Date();
    nextDateToReturn.setDate(nextDateToReturn.getDate() + nextDate);
    nextDateToReturn = getDateFormatted(nextDateToReturn);
    nextDateToReturn = nextDateToReturn.split(" ")[0] + " " + "00:00:00";
    return nextDateToReturn;
  }
};

const getDateFormattedDaysBefore = (d, n) => {
  const cd = new Date(d - n * 24 * 60 * 60 * 1000);
  MyDate = changeZone(cd);
  return (
    MyDate.getFullYear() +
    "-" +
    twoDigits(1 + MyDate.getMonth()) +
    "-" +
    twoDigits(MyDate.getDate()) +
    " " +
    twoDigits(MyDate.getHours()) +
    ":" +
    twoDigits(MyDate.getMinutes()) +
    ":" +
    twoDigits(MyDate.getSeconds())
  );
};

module.exports = {
  getDateFormatted,
  getDateMinuteFormatted,
  getDayFormatted,
  getDateOfDay,
  getDateFormattedDaysBefore,
};
