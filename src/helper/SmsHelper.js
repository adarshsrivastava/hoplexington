const send = async (contact, msg) => {
  // Send sms to user
  console.log(contact, msg)

  const accountSid = global.fastify.config.TW_ACCOUNT_SID // Your Account SID from www.twilio.com/console
  const authToken = global.fastify.config.TW_AUTH_TOKEN // Your Auth Token from www.twilio.com/console
  const validTwilioNumber = global.fastify.config.TW_TWILIO_NUMBER // Your Auth Token from www.twilio.com/console

  const Twilio = require('twilio')
  const client = new Twilio(accountSid, authToken)

  client.messages.create({
    body: msg,
    to: contact, // Text this number
    from: validTwilioNumber // From a valid Twilio number
  }).then((message) => {
    console.log('SMS SID : ', message.sid)
    // return true
  }).catch((err) => {
    console.log('SMS ERROR, : ', err)
    // return false
  })

  return true
}

module.exports = { send }
