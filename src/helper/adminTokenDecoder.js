const getRole = async (token) => {
  const hToken = token.split(' ')[1]
  const decoded = global.fastify.jwt.decode(hToken).role
  return decoded
}

module.exports = { getRole }
