const contact = async (token) => {
  const hToken = token.split(' ')[1]
  const decoded = global.fastify.jwt.decode(hToken).contact
  return decoded
}

module.exports = { contact }
