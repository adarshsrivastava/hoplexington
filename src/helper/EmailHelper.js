const nodemailer = require("nodemailer");

const send = async (contact, msg) => {
  // Send sms to user
  console.log(contact, msg);

  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: "api.happierhour@gmail.com",
      pass: "excnztohkwjfmmcy",
    },
  });
  const result = await transporter.sendMail({
    from: "api.happierhour@gmail.com",
    to: contact,
    subject:
      "Happier Hour - Your Venue Login passcode generated on " +
      new Date().toISOString().split("T")[0],
    html: msg,
  });

  return result;
};

module.exports = { send };
