const commonSchema = require("../common/authToken");

const sendOtp = {
  tags: ["admin"],
  summary: "Send OTP to admin contact",
  description:
    "Send admin contact as the parameter and it will check, if exists it'll send OTP to admin contact.",
  params: {
    type: "object",
    properties: {
      contact: {
        type: "string",
        description: "Enter admins contact number.",
      },
    },
  },
};

const login = {
  tags: ["admin"],
  summary: "Post admin contact and OTP",
  description:
    "Post a request with admin contact number and OTP to get an authorized token to access dashboard and etc.",
  body: {
    type: "object",
    properties: {
      contact: { type: "string" },
      otp: { type: "string" },
    },
  },
};
const venue_login = {
  tags: ["admin"],
  summary: "Post admin contact and OTP",
  description:
    "Post a request with admin contact number and OTP to get an authorized token to access dashboard and etc.",
  body: {
    type: "object",
    properties: {
      email: { type: "string" },
      otp: { type: "string" },
    },
  },
};

const isLogin = {
  tags: ["admin"],
  summary: "Check admin is logged in or not.",
  description:
    "Pass admin auth token in header will return admin is logged in or not.",
  headers: commonSchema.authToken,
};

const getPasses = {
  tags: ["Admin :: Passes"],
  summary:
    "You will get passes list with all tow parameter eg: daily specials, upcoming, Expired and block passes.",
  description:
    "Api needs auth token of authenticated user. And it will return you the data you needs. Here are two more optional parameter with query string. one for page number and one for passes type. It will return you array of object for showing passes.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    required: ["pass_type"],
    additionalProperties: false,
    properties: {
      pass_type: {
        type: "string",
        enum: ["daily", "upcoming", "expired", "block", "line_hop"],
        description:
          'Type of passes you need to get. that are => "daily, upcoming, expired, block". Enter daily for daily specials and enter upcoming for fetching upcoming passes. Default pass type will be daily if no any type selected.',
      },
      page_no: {
        type: "integer",
        description:
          "Enter page number. It is applied only when pass type is applied. If there is no any pass_type passed in query string, page number will ignored. And if page_no not passed default it will be 1.",
      },
      bar_id: {
        type: "integer",
        description:
          "Enter bar_id. It is applied only when pass type is applied. If there is no any bar_id passed in query string, bar_id  will ignored.",
      },
    },
  },
};

const addPass = {
  tags: ["Admin :: Passes"],
  summary: "Add new passes",
  description: "Fill the details properly. To add a new pass.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    required: ["name"],
    properties: {
      name: {
        type: "string",
        description: "Enter the name of new pass.",
      },
      tagline: {
        type: "string",
        description: "More about pass",
      },
      icon: {
        type: "string",
        description: "add the path of the icon of pass",
      },
      banner: {
        type: "string",
        description: "Enter the banner path of the pass",
      },
      saving: {
        type: "number",
        // format: 'double',
        description: "Enter the value of pass",
      },
      active_from: {
        type: "string",
        description: "Enter date and time from when the pass will be active",
      },
      valid_till: {
        type: "string",
        description:
          "Enter the date and time while this pass will be active. It is a required field",
      },
      additionals: {
        type: "string",
        description: "Describe more about this pass",
      },
      isPlus: {
        type: "number",
        description: "Enter true / false",
      },
      type: {
        type: "string",
        description: `'daily or upcoming', ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']`,
      },
      active_day: {
        type: "string",
        description: "Daily pass active day.",
      },
      is_recurring: {
        type: "number",
        description:
          "Enter 0 or 1 / true or false. Is recurring or not, 1 for recurring",
      },
      address_line_1: {
        type: "string",
        description: "Address line 1",
      },
      address_line_2: {
        type: "string",
        description: "Address line 2",
      },
      city: {
        type: "string",
        description: "City",
      },
      state: {
        type: "string",
        description: "State",
      },
      pin_code: {
        type: "string",
        description: "Enter pin code here",
      },
      latitude: {
        type: "string",
        description:
          "Enter latitude of the location. Where it will get redeemed",
      },
      longitude: {
        type: "string",
        description:
          "Enter latitude of the location. Where it will get redeemed",
      },
      pass_attributes: {
        type: "array",
        description: "Enter attributes of the pass",
        items: {
          type: "object",
          properties: {
            title: {
              type: "string",
            },
          },
        },
      },
    },
  },
};

const updatePass = {
  tags: ["Admin :: Passes"],
  summary: "Update details of existing passes.",
  description: "Update details of the pass using this route.",
  headers: commonSchema.authToken,
  params: {
    type: "object",
    required: ["id"],
    properties: {
      id: {
        type: "integer",
        description: "Field for passing pass id which will get update.",
      },
    },
  },
  body: {
    type: "object",
    required: ["name"],
    properties: {
      name: {
        type: "string",
        description: "Enter the name of new pass.",
      },
      tagline: {
        type: "string",
        description: "More about pass",
      },
      icon: {
        type: "string",
        description: "add the path of the icon of pass",
      },
      banner: {
        type: "string",
        description: "Enter the banner path of the pass",
      },
      saving: {
        type: "number",
        // format: 'double',
        description: "Enter the value of pass",
      },
      active_from: {
        type: "string",
        description: "Enter date and time from when the pass will be active",
      },
      valid_till: {
        type: "string",
        description:
          "Enter the date and time while this pass will be active. It is a required field",
      },
      additionals: {
        type: "string",
        description: "Describe more about this pass",
      },
      isPlus: {
        type: "number",
        description: "Enter true / false",
      },
      type: {
        type: "string",
        description: `'daily or upcoming', ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday']`,
      },
      active_day: {
        type: "string",
        description: "Daily pass active day.",
      },
      is_recurring: {
        type: "number",
        description:
          "Enter 0 or 1 / true or false. Is recurring or not, 1 for recurring",
      },
      address_line_1: {
        type: "string",
        description: "Address line 1",
      },
      address_line_2: {
        type: "string",
        description: "Address line 2",
      },
      city: {
        type: "string",
        description: "City",
      },
      state: {
        type: "string",
        description: "State",
      },
      pin_code: {
        type: "string",
        description: "Enter pin code here",
      },
      latitude: {
        type: "string",
        description:
          "Enter latitude of the location. Where it will get redeemed",
      },
      longitude: {
        type: "string",
        description:
          "Enter latitude of the location. Where it will get redeemed",
      },
      status: {
        type: "string",
        description: "Enter status eg: active or block.",
      },
      pass_attributes: {
        type: "array",
        description: "Enter attributes of the pass",
        items: {
          type: "object",
          properties: {
            title: {
              type: "string",
            },
          },
        },
      },
    },
  },
};

const users = {
  tags: ["admin"],
  summary: "Get registered users list",
  description:
    "Get registered users list. It will return first 10 user list after that you have to send page number for getting more users list.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    additionalProperties: false,
    properties: {
      page_no: {
        type: "integer",
        description: "Enter page number. default it will be 1.",
      },
    },
  },
};

const userDetails = {
  tags: ["admin"],
  summary: "Get registered users full details.",
  description: "Get a user's full details, With all redeemed passes. etc.",
  headers: commonSchema.authToken,
  params: {
    type: "object",
    required: ["id"],
    properties: {
      id: {
        type: "integer",
        description: "Pass user id for getting users full details.",
      },
    },
  },
};

const addMedia = {
  tags: ["admin"],
  summary: "Upload media.",
  description: "Upload an image/media file like passes icon or banner.",
  headers: commonSchema.authToken,
};

const getMedia = {
  tags: ["admin"],
  summary: "get media list of: icon and banner.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    additionalProperties: false,
    properties: {
      media_type: {
        type: "string",
        enum: ["icon", "banner"],
        description: "enter media_type icon or banner",
      },
      page_no: {
        type: "integer",
        description:
          "Enter page number. default 1 (Ignored if no any type chosen.)",
      },
    },
  },
};

const removeMedia = {
  tags: ["admin"],
  summary: "Remove media.",
  description:
    "Remove an image/media file only if it is not attached with any pass.",
  headers: commonSchema.authToken,
  params: {
    type: "object",
    required: ["id"],
    properties: {
      id: {
        type: "integer",
        description: "Media ID is required.",
      },
    },
  },
  body: {
    type: "object",
    properties: {
      url: {
        type: "string",
        description: "Enter full url for of media",
      },
    },
  },
};

const sendPushNotification = {
  tags: ["admin"],
  summary: "Send push notification.",
  description: "Admin can send push notification to users.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      title: { type: "string" },
      message: { type: "string" },
    },
  },
};

const schedulePushNotification = {
  tags: ["admin"],
  summary: "Schedule push notification.",
  description:
    "Admin can schedule push notification for an specific time. Which will send to Users on an entered time.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      title: { type: "string" },
      message: { type: "string" },
      scheduled_time: { type: "string" },
    },
  },
};

const getSchedulePushNotification = {
  tags: ["admin"],
  summary: "Get all scheduled push notification.",
  description: "Admin can see all schedule push notification.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    additionalProperties: false,
    properties: {
      type: {
        type: "string",
        enum: ["active", "admin", "pass", "expired"],
        description: "Default active will be shown",
      },
      page_no: {
        type: "integer",
        description: "Enter page number, Only integer value accepted.",
      },
    },
  },
};

const deleteSchedulePushNotification = {
  tags: ["admin"],
  summary: "Delete scheduled push notification.",
  description: "Admin can delete schedule push notification.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      id: {
        type: "integer",
        description: "Enter id of push notification",
      },
    },
  },
};

const createNewPaidUser = {
  tags: ["admin"],
  summary: "Create a user with paid privileges.",
  description:
    "Create a user with paid privileges, When user will register with this number will get a paid access.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      name: {
        type: "string",
        description: "Enter name of user",
      },
      contact: {
        type: "string",
        description: "Enter contact number of user",
      },
    },
  },
};

const selectPaidUserViaUser = {
  tags: ["admin"],
  summary: "Get user with paid privileges.",
  description: "Get user with paid privileges, created via admin.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    additionalProperties: false,
    properties: {
      page_no: {
        type: "integer",
        description: "Enter page number, Only integer value accepted.",
      },
    },
  },
};

const deleteNewPaidUser = {
  tags: ["admin"],
  summary: "Delete user with paid privileges.",
  description: "Delete user with paid privileges, created via admin.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      id: {
        type: "integer",
        description: "Enter id user",
      },
    },
  },
};

const getSwipesCount = {
  tags: ["admin"],
  summary: "Get count number per passes",
  description: "Get count number per passes (daily, monthly, weekly)",
  headers: commonSchema.authToken,
};

const getUsersCount = {
  tags: ["admin"],
  summary: "Get users count",
  description: "Get count with their paid status.",
  headers: commonSchema.authToken,
};

const updateRefCode = {
  tags: ["admin"],
  summary: "Update referral code",
  description: "Update ambassador referral code if not added.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      id: { type: "integer" },
      referral: { type: "string" },
    },
  },
};

const getReferrals = {
  tags: ["admin"],
  summary: "Get referrals",
  description: "Get refferals list.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    required: ["referral"],
    additionalProperties: false,
    properties: {
      referral: {
        type: "string",
      },
    },
  },
};

const markAsPaid = {
  tags: ["admin"],
  summary: "Mark as paid",
  description: "Update status as paid.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      id: { type: "integer" },
    },
  },
};

const all_locations = {
  tags: ["admin"],
  summary: "Get All Locations list",
  description:
    "Get All Locations  list. It will return first 10 location list after that you have to send page number for getting more location list.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    additionalProperties: false,
    properties: {
      page_no: {
        type: "integer",
        description: "Enter page number. default it will be 1.",
      },
    },
  },
};
const all_bars = {
  tags: ["admin"],
  summary: "Get All Bars list",
  description:
    "Get All Bars  list. It will return first 10 location list after that you have to send page number for getting more location list.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    additionalProperties: false,
    properties: {
      page_no: {
        type: "integer",
        description: "Enter page number. default it will be 1.",
      },
    },
  },
};

module.exports = {
  sendOtp,
  login,
  isLogin,
  venue_login,
  getPasses,
  addPass,
  updatePass,
  users,
  userDetails,
  addMedia,
  getMedia,
  removeMedia,
  sendPushNotification,
  schedulePushNotification,
  getSchedulePushNotification,
  deleteSchedulePushNotification,
  createNewPaidUser,
  selectPaidUserViaUser,
  deleteNewPaidUser,
  getSwipesCount,
  getUsersCount,
  updateRefCode,
  getReferrals,
  markAsPaid,
  //locations
  all_locations,
  //bars
  all_bars,
};
