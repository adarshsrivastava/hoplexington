const checkUserExists = {
  tags: ["user"],
  summary:
    "It will check is user exists or not and send a OTP to given contact number",
  description:
    "Put the contact number in query parameter and it will check the given contact is exists or not and also send an OTP to given contact. users exists or not check responses data. { isExists: 0 or 1 }, 0 for not exists and 1 for existing user.",
  params: {
    type: "object",
    properties: {
      contact: {
        type: "string",
        description:
          "Enter user contact number to check user is exists or not, also it will send an OTP to user.",
      },
    },
  },
};

const userLogin = {
  tags: ["user"],
  summary: "Post user contact and OTP",
  description:
    "Post a request with user contact number and OTP to get an authorized token to access dashboard and etc.",
  body: {
    type: "object",
    properties: {
      contact: { type: "string" },
      otp: { type: "string" },
      deviceID: { type: "string" },
    },
  },
};

const userRegister = {
  tags: ["user"],
  summary: "Register a new user.",
  description:
    "Register a new user with contact number, OTP, firstName and lastName",
  body: {
    type: "object",
    properties: {
      contact: { type: "string" },
      otp: { type: "string" },
      firstName: { type: "string" },
      dob: { type: "string" },
      deviceID: { type: "string" },
      referral: { type: "string" },
    },
  },
};

const verifyNewUserOtp = {
  tags: ["user"],
  summary: "Verify OTP, While registering a new user.",
  description:
    "Verify user OTP when user trying to register. After verifying user will enter their first name and other details.",
  body: {
    type: "object",
    properties: {
      contact: { type: "string" },
      otp: { type: "string" },
    },
  },
};

const verifyReferral = {
  tags: ["user"],
  summary: "Verify referral code.",
  description:
    "Before register if you are using referral code please make sure it is valid or not.",
  body: {
    type: "object",
    properties: {
      referral: { type: "string" },
    },
  },
};

const rmUser = {
  tags: ["user"],
  summary: "Remove an existing user.",
  description:
    "It is only for testing purpose. Remove an existing user using there contact number.",
  params: {
    type: "object",
    properties: {
      contact: {
        type: "string",
        description: "Enter contact number to remove an existing user.",
      },
    },
  },
};

module.exports = {
  checkUserExists,
  userLogin,
  userRegister,
  verifyNewUserOtp,
  verifyReferral,
  rmUser,
};
