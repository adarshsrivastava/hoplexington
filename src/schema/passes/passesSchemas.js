const commonSchema = require("../common/authToken");

const getPasses = {
  tags: ["passes"],
  summary:
    "You will get passes list with all tow parameter eg: daily_specials and upcoming.",
  description:
    "Api needs auth token of authenticated user. And it will return you the data you needs. Here are two more optional parameter with query string. one for page number and one for passes type. It will return you array of object for showing passes to user.",
  headers: commonSchema.authToken,
  querystring: {
    type: "object",
    // required: ['pass_type'],
    additionalProperties: false,
    properties: {
      pass_type: {
        type: "string",
        enum: ["daily", "upcoming", "line_hop"],
        description:
          'Type of passes you need to get. that are => "daily, upcoming". Enter daily for daily specials and enter upcoming for fetching upcoming passes.',
      },
      page_no: {
        type: "integer",
        description:
          "Enter page number. It is applied only when pass type is applied. If there is no any pass_type passed in query string, page number will ignored. And if page_no not passed default it will be 1.",
      },
      location_id: {
        type: "integer",
        description: "Enter location id",
      },
    },
  },
};

const getPassDetails = {
  tags: ["passes"],
  summary:
    "You will get passes list with all tow parameter eg: daily_specials and upcoming.",
  description:
    "Api needs auth token of authenticated user. And it will return you the data you needs. Here are two more optional parameter with query string. one for page number and one for passes type. It will return you array of object for showing passes to user. And also return my_rating with your rating on this pass, rated_ad is the date when you rated the same pass. And also you will receive total number of rating as total_rating_count and average rating on the pass as avg_rating.",
  headers: commonSchema.authToken,
  params: {
    type: "object",
    properties: {
      id: {
        type: "string",
        description: "Enter Pass Id for getting full details.",
      },
    },
  },
};
const deletePassDetails = {
  tags: ["passes"],
  summary: "You will delete passes",
  description:
    "Api needs auth token of authenticated user. And it will return you the data you needs. Here are two more optional parameter with query string. one for page number and one for passes type. It will return you array of object for showing passes to user. And also return my_rating with your rating on this pass, rated_ad is the date when you rated the same pass. And also you will receive total number of rating as total_rating_count and average rating on the pass as avg_rating.",
  headers: commonSchema.authToken,
  params: {
    type: "object",
    properties: {
      id: {
        type: "string",
        description: "Enter Pass Id for deleting.",
      },
    },
  },
};
module.exports = {
  getPasses,
  getPassDetails,
  deletePassDetails,
};
