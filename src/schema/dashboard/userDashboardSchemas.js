const commonSchema = require("../common/authToken");
const profile = {
  tags: ["user dashboard"],
  summary: "Get user profile and user all personal details",
  description:
    "Using authorization token, Get user profile and user all personal details",
  headers: commonSchema.authToken,
};

const getRedeemedPasses = {
  tags: ["user dashboard"],
  summary: "Get user passes list which are redeemed by user.",
  description:
    "Using authorization token, Get user passes which are redeemed by user.",
  headers: commonSchema.authToken,
};

const redeemPass = {
  tags: ["user dashboard"],
  summary: "Redeem a pass.",
  description:
    "Using authorization token, Redeem a new pass for user by posting pass_id.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    required: ["pass_id"],
    properties: {
      pass_id: {
        type: "integer",
        description: "Enter pass_id for redeem for it.",
      },
    },
  },
};

const updatePushNotification = {
  tags: ["user dashboard"],
  summary: "Update push notification status.",
  description:
    "By sending notification status update users push notification status. (1, 0 ), Where 1 => active  and 0 => Notification blocked.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    required: ["status"],
    properties: {
      status: {
        type: "integer",
        description: "Update notification status in bool 0 or 1.",
      },
    },
  },
};

const ratePass = {
  tags: ["user dashboard"],
  summary: "Rate a pass or update users existing rating.",
  description:
    "By posting user token, pass_id and number of rating. It will rate a pass and also if you have already rated a pass user can change rating value via sending a new rating value.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    required: ["pass_id", "rate"],
    properties: {
      pass_id: {
        type: "integer",
        description: "Send pass id which will be rate.",
      },
      rate: {
        type: "integer",
        description: "Send rate value min 1, and max 5",
      },
    },
  },
};

const updateFcmToken = {
  tags: ["user dashboard"],
  summary: "Update USER FCM Token for push notification.",
  description: "Send FCM token. Will add User's FCM Token if not added.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      fcmToken: { type: "string" },
    },
  },
};

const removeFcmToken = {
  tags: ["user dashboard"],
  summary: "Remove USERs FCM Token from push notification.",
  description: "Send FCM token. Will remove User's FCM Token if added.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      fcmToken: { type: "string" },
    },
  },
};

const checkIfPaidViaAdmin = {
  tags: ["user dashboard"],
  summary: "Check if an user if get paid via admin.",
  description:
    "Send your authorization token to check if your is paid via admin.",
  headers: commonSchema.authToken,
};

const updatePaidStatus = {
  tags: ["user dashboard"],
  summary: "Update user paid status.",
  description: "Send your authorization token and paid status true of false.",
  headers: commonSchema.authToken,
  body: {
    type: "object",
    properties: {
      status: { type: "integer" },
    },
  },
};

module.exports = {
  profile,
  getRedeemedPasses,
  redeemPass,
  updatePushNotification,
  ratePass,
  updateFcmToken,
  removeFcmToken,
  checkIfPaidViaAdmin,
  updatePaidStatus,
};
