
const {StatusCodes} = require('http-status-codes');
const  {error_schema,forbidden,unprocessable_entity,bad_request,not_found} = require("./error");
const { successful_response,successful_with_no_data_response } = require('./success');

const response_mediums={
    [StatusCodes.OK]: successful_response,
    [StatusCodes.NO_CONTENT]: successful_with_no_data_response,
    [StatusCodes.FORBIDDEN]: forbidden,
    [StatusCodes.UNPROCESSABLE_ENTITY]: unprocessable_entity,
    [StatusCodes.BAD_REQUEST]: bad_request,
    [StatusCodes.NOT_FOUND]: not_found,
    [StatusCodes.INTERNAL_SERVER_ERROR]: error_schema,
}

module.exports={...response_mediums};
