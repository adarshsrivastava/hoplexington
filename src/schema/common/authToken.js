const authToken = {
  type: 'object',
  properties: {
    authorization: {
      type: 'string',
      description: 'Enter authentication token'
    }
  },
  required: ['authorization']
}

module.exports = {
  authToken
}
