const fp = require('fastify-plugin')
const adminToken = require('../../helper/adminTokenDecoder')

module.exports = fp(async function(fastify, options, next) {
    fastify.decorate('isBar', async function(request, reply) {
        try {
            // decode header and check role
            const role = await adminToken.getRole(request.headers.authorization)
            console.log(role)
            if (role !== 'bar') {
                reply.status(401).send({
                    success: false,
                    error: 'Unauthorized',
                    message: 'You do not have current privileges to access this panel. Try to login with admin credentials.'
                })
            }
        } catch (err) {
            reply.send(err)
        }
    })

    next()
})