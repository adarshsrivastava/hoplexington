const bar_model = require("../../models/bar.model");
const createBar = async (request, reply) => {
  await protected_from_venue(request, reply, true);
  console.log(request);
  // run a model
  const new_bar = {
    ...request.body,
  };
  try {
    const return_data = await bar_model.create(new_bar);
    return reply.send({ data: return_data });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const updateBar = async (request, reply) => {
  await protected_from_venue(request, reply, false, request.params.id);
  try {
    delete request.body.location;
    const update_document = {
      ...request.body,
    };
    bar = await bar_model.update({ id: request.params.id }, update_document);
    // return the response here
    return reply.send({ data: { bar } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const getBars = async (request, reply) => {
  await protected_from_venue(request, reply, false);
  try {
    bars = await bar_model.findManyWithRelationship(
      {},
      {
        location: true,
      }
    );
    // return the response here
    return reply.send({ data: { bars } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};
const getBarById = async (request, reply) => {
  await protected_from_venue(request, reply, false, request.params.id);
  try {
    bars = await bar_model.findManyWithRelationship(
      { id: request.params.id },
      {
        location: true,
      }
    );
    // return the response here
    return reply.send({ data: { bars } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const protected_from_venue = async (
  request,
  reply,
  harshness = false,
  asking = null
) => {
  if (request.user.role === "venue") {
    if (harshness) {
      // return immediately , NO UPDATE OPERATION PERMITTED
      return reply.code(422).send({
        error: {},
        message: "ACTION NOT PERMITTED FOR VENUE ROLE USER.",
      });
    }
    if (asking !== null) {
      // see if asking_id is in the assigned id or not
      const ids = [
        ...new Set(request.user.associated_venue_ids.map((a) => a.id)),
      ];
      // If asked id id equal to allotted id then its good
      if (!ids.includes(asking)) {
        return reply.code(422).send({
          error: {},
          message: "ACTION NOT PERMITTED FOR VENUE ROLE USER.",
        });
      }
    }
  }
};

module.exports = {
  createBar,
  updateBar,
  getBars,
  getBarById,
};
