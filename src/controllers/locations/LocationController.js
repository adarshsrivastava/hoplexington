const location_model = require("../../models/location.model");
const bars_model = require("../../models/bar.model");
const user_model = require("../../models/users.model");

const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient();

const createLocation = async (request, reply) => {
  await protected_from_venue(request, reply, true);

  // run a model
  const new_location = {
    ...request.body,
  };
  try {
    const return_data = await location_model.create(new_location);
    return reply.send({ data: return_data });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const updateLocation = async (request, reply) => {
  await protected_from_venue(request, reply, true);
  try {
    const update_document = {
      ...request.body,
    };
    location = await location_model.update(
      { id: request.params.id },
      update_document
    );
    // return the response here
    return reply.send({ data: { location } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const getLocations = async (request, reply) => {
  console.log("Get All Locations");
  await protected_from_venue(request, reply, true);
  try {
    locations = await location_model.findMany({
      where: {
        active: {
          equals: true, // Default mode
        },
      },
    });
    // return the response here
    return reply.send({ data: { locations } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};
const getLocationById = async (request, reply) => {
  await protected_from_venue(request, reply, false, request.params.id);
  try {
    location = await location_model.findUnique({ id: request.params.id });

    const location_id = request.params.id;
    const paying_users =
      await prisma.$queryRaw`SELECT count(id) as hop_plus_users FROM users WHERE is_hop_plus=1 AND location_id= ${location_id}`;
    location.paying_users = paying_users[0] ?? { hop_plus_users: 0 };
    // return the response here
    return reply.send({ data: { location } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const getBarsOnLocationById = async (request, reply) => {
  await protected_from_venue(request, reply, false, request.params.id);
  try {
    location = await location_model.findUnique(
      { id: request.params.id },
      {
        bars: true,
      }
    );
    // return the response here
    return reply.send({ data: { location } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const getPassesOnBarsById = async (request, reply) => {
  try {
    bars = await bars_model.findUnique(
      { id: request.params.id },
      {
        passes: true,
      }
    );
    // return the response here
    return reply.send({ data: { bars } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const getPassesOnLocationById = async (request, reply) => {
  await protected_from_venue(request, reply, false, request.params.id);
  try {
    location = await location_model.findUnique(
      { id: request.params.id },
      {
        passes: true,
      }
    );
    // return the response here
    return reply.send({ data: { location } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const updateLocationForUser = async (request, reply) => {
  await protected_from_venue(request, reply, true);
  try {
    const update_document = {
      ...request.body,
    };
    user = await user_model.updateWithUnique(
      { contact: request.user.contact },
      update_document
    );

    delete user.otp;
    delete user.device_ids;
    // return the response here
    return reply.send({ data: { user } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const protected_from_venue = async (
  request,
  reply,
  harshness = false,
  asking = null
) => {
  if (request.user.role === "venue") {
    if (harshness) {
      // return immediately , NO UPDATE OPERATION PERMITTED
      return reply.code(422).send({
        error: {
          message: "ACTION NOT PERMITTED FOR VENUE ROLE USER.",
        },
        message: "ACTION NOT PERMITTED FOR VENUE ROLE USER.",
      });
    }
    if (asking !== null) {
      // see if asking_id is in the assigned id or not
      const location_ids = [
        ...new Set(request.user.associated_venue_ids.map((a) => a.location_id)),
      ];
      // If asked id id equal to allotted id then its good
      if (!location_ids.includes(asking)) {
        return reply.code(422).send({
          error: {},
          message: "ACTION NOT PERMITTED FOR VENUE ROLE USER.",
        });
      }
    }
  }
};
module.exports = {
  createLocation,
  updateLocation,
  getLocations,
  getLocationById,
  getBarsOnLocationById,
  getPassesOnBarsById,
  getPassesOnLocationById,
  updateLocationForUser,
};
