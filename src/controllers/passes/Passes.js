const my_passes_model = require("../../models/my_passes.model");
const user_model = require("../../models/users.model");

const passes = require("../../models/passes.model");
const pass_attributes = require("../../models/pass_attributes.model");
// Imported Custom Helpers
const DateFormat = require("../../helper/DateFormat");

const deletePass = async (request, reply) => {
  try {
    const update_document = {
      status: "block",
    };
    pass = await passes.update({ id: request.params.id }, update_document);
    // return the response here
    return reply.send({ data: { pass } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const getPasses = async (request, reply) => {
  const status = "active";

  const current_date = DateFormat.getDateMinuteFormatted(new Date());
  const current_day = DateFormat.getDayFormatted(new Date());

  const pass_type = request.params.pass_type;
  const location_id = request.query.location_id;

  var conditions = {};

  // run the model
  conditions = {
    OR: [
      {
        valid_till: {
          gte: new Date(current_date), // Default mode
        },
      },
      { is_recurring: true },
    ],
  };

  switch (pass_type) {
    case "daily":
      var daily = await getData(
        conditions,
        "daily",
        status,
        location_id,
        request
      );
      return reply.send({ data: { passes: daily } });
      break;
    case "upcoming":
      var upcoming = await getData(
        conditions,
        "upcoming",
        status,
        location_id,
        request
      );
      return reply.send({ data: { passes: upcoming } });
      break;
    case "line_hop":
      var line_hop = await getData(
        conditions,
        "line_hop",
        status,
        location_id,
        request
      );
      return reply.send({ data: { passes: line_hop } });
      break;
    default:
      var daily = await getData(
        conditions,
        "daily",
        status,
        location_id,
        request
      );
      var upcoming = await getData(
        conditions,
        "upcoming",
        status,
        location_id,
        request
      );
      var line_hop = await getData(
        conditions,
        "line_hop",
        status,
        location_id,
        request
      );
      return reply.send({ data: { passes: { daily, upcoming, line_hop } } });
      break;
  }
};
const getData = async (conditions, pass_type, status, location_id, request) => {
  const now = new Date();
  const startOfWeek = new Date(
    now.getFullYear(),
    now.getMonth(),
    now.getDate() - now.getDay()
  );

  const combined_conditions = {
    type: {
      equals: pass_type, // Default mode
    },
    status: {
      equals: status, // Default mode
    },
    location_id: {
      equals: location_id, // Default mode
    },
    ...conditions, // any other condition
  };
  return await passes.findManyWithRelationship(combined_conditions, {
    pass_attributes: true,
    bar: true,
    location: true,
    my_passes: {
      where: {
        user_id: request.user.payload.id,
        created_at: {
          gt: new Date(startOfWeek),
        },
      },
    },
    redeemed_passed: {
      where: {
        user_id: request.user.payload.id,
        redeemed_at: {
          gt: new Date(startOfWeek),
        },
      },
    },
  });
};

module.exports = {
  getPasses,
  deletePass,
};
