const DateFormat = require("../../helper/DateFormat");
const decoder = require("../../helper/AuthTokenDecoder");

const getPasses = async (req, reply) => {
  try {
    // type are daily, upcoming

    const status = "active";
    const currentDate = DateFormat.getDateFormatted(new Date());
    const currentDay = DateFormat.getDayFormatted(new Date());

    const passType = req.query.pass_type;

    const location_id = req.query.location_id ? req.query.location_id : -1;
    const conditional_location_id_query =
      location_id === -1 ? "" : " AND location_id=" + location_id + " ";
    console.log(conditional_location_id_query);
    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    const pageLimit = 50;

    const upcomingCurrentDateStart = currentDate.split(" ")[0] + " 00:00:00";
    const upcomingCurrentDateEnd = currentDate.split(" ")[0] + " 23:59:59";

    // Check if any filter's are applied
    if (passType && passType !== "") {
      pageLimitStart = (pageNo - 1) * pageLimit;
      const connection = await global.fastify.mysql.getConnection();

      if (passType === "daily") {
        const [passes] = await connection.query(
          'SELECT  `id`, `name`, `tagline`, `icon`, `banner`, `saving`, DATE_FORMAT(`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `additionals`, `isPlus`, `type`, `active_day`, `is_recurring`, `address_line_1`, `address_line_2`, `city`, `state`, `pin_code`, `latitude`, `longitude`, `status`, DATE_FORMAT(`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at` FROM `passes` WHERE `active_from` <= ? AND `valid_till` >= ? AND `type` = ? AND `status` = ? OR `id` IN (SELECT `id` FROM `passes` WHERE `type` = "' +
            passType +
            '" AND `is_recurring` = 1 AND `active_day` = "' +
            currentDay +
            '" AND `status` = "' +
            status +
            '")  ' +
            conditional_location_id_query +
            " ORDER BY `active_from` DESC, `id` DESC LIMIT ?, ?",
          [
            currentDate,
            currentDate,
            passType,
            status,
            pageLimitStart,
            pageLimit,
          ]
        );

        connection.release();

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            pass_type: passType,
            passes,
          },
        });
      } else if (passType === "line_hop") {
        const [passes] = await connection.query(
          'SELECT  `id`, `name`, `tagline`, `icon`, `banner`, `saving`, DATE_FORMAT(`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `additionals`, `isPlus`, `type`, `active_day`, `is_recurring`, `address_line_1`, `address_line_2`, `city`, `state`, `pin_code`, `latitude`, `longitude`, `status`, DATE_FORMAT(`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at` FROM `passes` WHERE `active_from` <= ? AND `valid_till` >= ? AND `type` = ? AND `status` = ? OR `id` IN (SELECT `id` FROM `passes` WHERE `type` = "' +
            passType +
            '" AND `status` = "' +
            status +
            '") ' +
            conditional_location_id_query +
            " ORDER BY `active_from` DESC, `id` DESC LIMIT ?, ?",
          [
            currentDate,
            currentDate,
            passType,
            status,
            pageLimitStart,
            pageLimit,
          ]
        );

        connection.release();

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            pass_type: passType,
            passes,
          },
        });
      } else if (passType === "upcoming") {
        // Decode token
        const authContact = await decoder.contact(req.headers.authorization);
        const [userD] = await connection.query(
          "SELECT `id` FROM `users` where `contact` = ?",
          [authContact]
        );
        let userId = 0;
        if (userD.length > 0) {
          userId = userD[0].id;
        }

        const [passes] = await connection.query(
          'SELECT `passes`.`id`, `passes`.`name`, `passes`.`tagline`, `passes`.`icon`, `passes`.`banner`, `passes`.`saving`, DATE_FORMAT(`passes`.`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`passes`.`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `passes`.`additionals`, `passes`.`isPlus`, `passes`.`type`, `passes`.`active_day`, `passes`.`is_recurring`, `passes`.`address_line_1`, `passes`.`address_line_2`, `passes`.`city`, `passes`.`state`, `passes`.`pin_code`, `passes`.`latitude`, `passes`.`longitude`, `passes`.`status`, DATE_FORMAT(`passes`.`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`passes`.`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at`, IF(`redeemed_passed`.`user_id` = ?, 1, 0) as `is_redeemed` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` WHERE `passes`.`active_from` >= ? AND `passes`.`valid_till` <= ? AND `passes`.`valid_till` >= ? AND `passes`.`status` = ? AND `passes`.`type` = ?  ' +
            conditional_location_id_query +
            " ORDER BY `passes`.`active_from` ASC LIMIT ?, ?",
          [
            userId,
            upcomingCurrentDateStart,
            upcomingCurrentDateEnd,
            currentDate,
            status,
            passType,
            pageLimitStart,
            pageLimit,
          ]
        );
        connection.release();

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            pass_type: passType,
            passes,
          },
        });
      }
    } else {
      // If there is no any filter applied / Send both data

      const connection = await global.fastify.mysql.getConnection();
      let dPassType = "daily";
      const [daily] = await connection.query(
        'SELECT  `id`, `name`, `tagline`, `icon`, `banner`, `saving`, DATE_FORMAT(`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `additionals`, `isPlus`, `type`, `active_day`, `is_recurring`, `address_line_1`, `address_line_2`, `city`, `state`, `pin_code`, `latitude`, `longitude`, `status`, DATE_FORMAT(`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at` FROM `passes` WHERE `active_from` <= ? AND `valid_till` >= ? AND `type` = ? AND `status` = ? OR `id` IN (SELECT `id` FROM `passes` WHERE `type` = "' +
          dPassType +
          '" AND `is_recurring` = 1 AND `active_day` = "' +
          currentDay +
          '" AND `status` = "' +
          status +
          '")  ' +
          conditional_location_id_query +
          " ORDER BY `active_from` DESC, `id` DESC LIMIT ?, ?",
        [currentDate, currentDate, dPassType, status, pageLimitStart, pageLimit]
      );

      const authContact = await decoder.contact(req.headers.authorization);
      const [userD] = await connection.query(
        "SELECT `id` FROM `users` where `contact` = ?",
        [authContact]
      );
      let userId = 0;
      if (userD.length > 0) {
        userId = userD[0].id;
      }

      dPassType = "upcoming";
      // const [upcoming] = await connection.query(
      //   'SELECT `passes`.`id`, `passes`.`name`, `passes`.`tagline`, `passes`.`icon`, `passes`.`banner`, `passes`.`saving`, DATE_FORMAT(`passes`.`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`passes`.`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `passes`.`additionals`, `passes`.`isPlus`, `passes`.`type`, `passes`.`active_day`, `passes`.`is_recurring`, `passes`.`address_line_1`, `passes`.`address_line_2`, `passes`.`city`, `passes`.`state`, `passes`.`pin_code`, `passes`.`latitude`, `passes`.`longitude`, `passes`.`status`, DATE_FORMAT(`passes`.`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`passes`.`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at`, IF(`redeemed_passed`.`user_id` = ?, 1, 0) as `is_redeemed` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` WHERE `passes`.`active_from` >= ? AND `passes`.`valid_till` <= ? AND `passes`.`valid_till` >= ? AND `passes`.`status` = ? AND `passes`.`type` = ?  ' +
      //     conditional_location_id_query +
      //     " ORDER BY `passes`.`active_from` ASC LIMIT ?, ?",
      //   [
      //     userId,
      //     upcomingCurrentDateStart,
      //     upcomingCurrentDateEnd,
      //     currentDate,
      //     status,
      //     dPassType,
      //     pageLimitStart,
      //     pageLimit,
      //   ]
      // );

      const [upcoming] = await connection.query(
        "SELECT passes.*, IF(`redeemed_passed`.`user_id` = ?, 1, 0) as `is_redeemed` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id`" +
          "WHERE `passes`.`active_from` >= ? AND `passes`.`valid_till` <= ? AND `passes`.`status` = ? AND `passes`.`type` = ?  " +
          conditional_location_id_query +
          " ORDER BY `passes`.`active_from` ASC LIMIT ?, ?",
        [
          userId,
          upcomingCurrentDateStart,
          upcomingCurrentDateEnd,
          status,
          dPassType,
          pageLimitStart,
          pageLimit,
        ]
      );
      dPassType = "line_hop";
      const [line_hop_passes] = await connection.query(
        'SELECT  `id`, `name`, `tagline`, `icon`, `banner`, `saving`, DATE_FORMAT(`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `additionals`, `isPlus`, `type`, `active_day`, `is_recurring`, `address_line_1`, `address_line_2`, `city`, `state`, `pin_code`, `latitude`, `longitude`, `status`, DATE_FORMAT(`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at` FROM `passes`' +
          ' WHERE `active_from` <= ? AND `valid_till` >= ? AND `type` = ? AND `status` = ? OR `id` IN (SELECT `id` FROM `passes` WHERE `type` = "' +
          dPassType +
          '" AND `status` = "' +
          status +
          '")  ' +
          conditional_location_id_query +
          " ORDER BY `active_from` DESC, `id` DESC LIMIT ?, ?",
        [currentDate, currentDate, dPassType, status, pageLimitStart, pageLimit]
      );

      connection.release();

      reply.status(200).send({
        success: true,
        message: "Passes fetched successfully.",
        date_start: upcomingCurrentDateStart,
        date_end: currentDate,
        current_time: currentDate,
        data: {
          page_no: pageNo,
          daily,
          upcoming,
          line_hop_passes,
        },
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Oops!",
      message: "Oops! Something went wrong.",
    });
  }
};

const getPassDetails = async (req, reply) => {
  try {
    const id = req.params.id;

    const authContact = await decoder.contact(req.headers.authorization);

    const connection = await global.fastify.mysql.getConnection();

    // User rating on same pass
    let myRating = [];
    let isRedeemedByUser = 0;
    const [rows] = await connection.query(
      "SELECT `id` FROM `users` where `contact` = ?",
      [authContact]
    );
    if (rows.length > 0) {
      const userId = rows[0].id;
      const [mRating] = await connection.query(
        'SELECT `rate`, DATE_FORMAT(`created_at`, "%Y-%m-%d %H:%i:%s") as `rated_at` FROM `pass_ratings` WHERE `pass_id` = ? AND `user_id` = ?',
        [id, userId]
      );

      myRating = mRating[0];
    }

    // Getting pass details
    const [passes] = await connection.query(
      'SELECT  `id`, `name`, `tagline`, `icon`, `banner`, `saving`, DATE_FORMAT(`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `additionals`, `isPlus`, `type`, `active_day`, `is_recurring`, `address_line_1`, `address_line_2`, `city`, `state`, `pin_code`, `latitude`, `longitude`, `status`, DATE_FORMAT(`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at` FROM `passes` WHERE `id` = ?',
      [id]
    );
    const [attr] = await connection.query(
      "SELECT `title` FROM `pass_attributes` WHERE `pass_id` = ?",
      [id]
    );

    // Total rating on that same pass
    const [totalRate] = await connection.query(
      "SELECT count(`id`) as `total_rating_count`, CAST (AVG(`rate`) as DECIMAL (2,1)) as `avg_rating` FROM `pass_ratings` WHERE `pass_id` = ?",
      [id]
    );

    // Total rating on that same pass
    const [totalSwipes] = await connection.query(
      "SELECT count(`id`) as `total_swipes_count` FROM `redeemed_passed` WHERE `pass_id` = ?",
      [id]
    );

    if (rows.length > 0) {
      const userId = rows[0].id;
      const [redeemed] = await connection.query(
        "SELECT count(`id`) AS `is_redeemed` FROM `redeemed_passed` WHERE `user_id` = ? AND `pass_id` = ?",
        [userId, id]
      );
      isRedeemedByUser = redeemed[0].is_redeemed;
    }
    connection.release();

    const data = passes[0];
    data.attributes = attr;
    data.my_rating = myRating;
    data.total_rating = totalRate[0];
    data.total_swiped = totalSwipes[0].total_swipes_count;
    data.is_redeemed = isRedeemedByUser;

    reply.status(200).send({
      success: true,
      message: "Pass details fetched successfully.",
      data,
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Oops!",
      message: "Oops! Something went wrong. " + error,
    });
  }
};

module.exports = {
  getPasses,
  getPassDetails,
};
