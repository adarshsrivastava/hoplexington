const my_passes_model = require("../../models/my_passes.model");
const user_model = require("../../models/users.model");
const passes = require("../../models/passes.model");
const payment_request = require("../../models/payment_requests.model");
const redeemed_passes = require("../../models/redeemed_passed.model");

const payments = require("../../models/payments.model");
const addToMyPasses = async (request, reply) => {
  // run the model

  const upsert_data = {
    pass_id: request.body.pass_id,
    user_id: request.user.payload.id,
  };

  try {
    const my_passes = AddToMyPass(upsert_data, "normal");
    return reply.send({ data: { my_passes: my_passes } });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const AddToMyPass = async (upsert_data, pass_type = "normal") => {
  const whereClause = {
    user_passes_identifier: {
      pass_id: upsert_data.pass_id,
      user_id: upsert_data.user_id,
    },
  };

  var data = { ...upsert_data };
  data.pass_type = pass_type;
  data.created_at = new Date();

  const my_passes = my_passes_model.upsert(whereClause, data, data);
  return my_passes;
};

const RemoveFromRedeemedPasses = async (where_data) => {
  const whereClause = {
    where: {
      pass_id: where_data.pass_id,
      user_id: where_data.user_id,
    },
  };

  const deleted = redeemed_passes.delete_selected_rows(whereClause);
  return deleted;
};

const getMyPassesOfUser = async (request, reply) => {
  // try {
  const now = new Date();
  const startOfWeek = new Date(
    now.getFullYear(),
    now.getMonth(),
    now.getDate() - now.getDay()
  );
  const where_condition = {
    created_at: {
      gt: new Date(startOfWeek),
    },
  };

  const user = await user_model.findManyWithRelationship(
    { id: request.user.payload.id },
    {
      my_passes: {
        where: where_condition,
        include: {
          pass: {
            include: {
              pass_attributes: true,
              bar: true,
              payments: {
                where: {
                  user_id: request.user.payload.id,
                },
              },
              redeemed_passed: {
                where: {
                  user_id: request.user.payload.id,
                },
              },
            },
          },
        },
      },
    },
    {
      created_at: "desc",
    }
  );
  // return the response here
  return reply.send({ data: { user } });
  // } catch (error) {
  //   return reply.code(422).send({ error: { ...error } });
  // }
};
//stripe create token

const create_token = async (request, reply) => {
  // find the pass id price.
  const txn_id = "TXN-" + Date.now();

  const pass = await passes.findUnique(
    {
      id: request.body.pass_id,
    },
    null
  );
  pass.quantity = request.body.quantity;

  if (pass.type != "line_hop") {
    return reply
      .code(422)
      .send({ error: { message: "Pass Should be Line HOP Pass" } });
  }
  const payment_request_payload = {
    amount: pass.saving * request.body.quantity * 100,
    currency: "usd",
    automatic_payment_methods: {
      enabled: true,
    },
    description: txn_id + " Payment",
    metadata: pass,
    shipping: {
      name: request.user.payload.first_name,
      address: {
        line1: request.user.payload.location.name || "No Address Line 1 ",
        line2: request.user.payload.location.name || "No Address Line 2 ",
        country: "United States",
      },
    },
  };
  const paymentIntent = await global.stripe.paymentIntents.create(
    payment_request_payload
  );

  const new_payment_request_intent = {
    user_id: request.user.payload.id,
    pass_id: request.body.pass_id,
    charge_intent: paymentIntent.id,
    quantity: request.body.quantity,
    charge_amount: pass.saving * request.body.quantity,
    charge_method: "stripe",
    transaction_id: txn_id,
    charge_response: JSON.stringify(paymentIntent),
    charge_request: JSON.stringify(payment_request_payload),
    charge_state: "in_process",
  };
  try {
    const return_data = await payment_request.create(
      new_payment_request_intent
    );
    return_data.charge_response = JSON.parse(return_data.charge_response);
    return_data.charge_request = JSON.parse(return_data.charge_request);

    return reply.send({ data: return_data });
  } catch (error) {
    return reply.code(422).send({ error: { ...error } });
  }
};

const captureCharge = async (request, reply) => {
  // view pass id and charge_token, get method ..
  // charge
  // save in my_passes
  // try {
  switch (request.body.charge_route) {
    case "apple":
      return reply
        .code(422)
        .send({ error: { message: "No Such Method for now" } });
      break;
    case "stripe":
      const transaction = await payment_request.findUnique(
        {
          transaction_id: request.body.transaction_id,
        },
        null
      );
      if (!transaction) {
        return reply
          .code(422)
          .send({ error: { message: "No Such Transaction for now" } });
      }
      transaction.charge_response = JSON.parse(transaction.charge_response);
      transaction.charge_request = JSON.parse(transaction.charge_request);

      const paymentIntent = await stripe.paymentIntents.retrieve(
        transaction.charge_intent
      );
      const payment = await process_stripe_payment(
        request,
        paymentIntent,
        transaction.transaction_id
      );
      const upsert_into_my_passes_data = {
        pass_id: parseInt(paymentIntent.metadata.id),
        user_id: parseInt(request.user.payload.id),
      };
      AddToMyPass(upsert_into_my_passes_data, "purchased");

      //RemoveFromRedeemedPasses(upsert_into_my_passes_data);

      return reply.send({ data: { payment } });
      break;
    default:
      return reply
        .code(422)
        .send({ error: { message: "This Method doesnt exist for now" } });
      break;
  }
  // } catch (error) {
  //   return reply.code(422).send({ error: { ...error } });
  // }
};

const process_stripe_payment = async (
  request,
  payment_intent_data,
  transaction_id
) => {
  const new_payment_intent = {
    user_id: parseInt(request.user.payload.id),
    pass_id: parseInt(payment_intent_data.metadata.id),
    charge_amount: payment_intent_data.amount,
    charge_method: "stripe",
    transaction_id: transaction_id,
    charge_valid: payment_intent_data.status === "succeeded" ? true : false,
    charge_response: JSON.stringify(payment_intent_data),
    charge_state: payment_intent_data.status,
    quantity: parseInt(payment_intent_data.metadata.quantity),
  };
  let whereClause = {
    transaction_id: transaction_id,
  };
  return await payments.upsert(
    whereClause,
    new_payment_intent,
    new_payment_intent
  );
};

module.exports = {
  addToMyPasses,
  getMyPassesOfUser,
  captureCharge,
  create_token,
};
