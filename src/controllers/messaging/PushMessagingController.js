const DateFormat = require("../../helper/DateFormat");

const sendNotification = async() => {
    const nowTime = DateFormat.getDateFormatted(new Date());

    console.log("Sending push notification on: ", nowTime);

    const connection = await global.fastify.mysql.getConnection();
    // Get current scheduled notification form DB
    let [rows] = await connection.query(
        "SELECT `title`, `message`, `pass_id`, `scheduled_for` FROM `scheduled_notifications` WHERE `scheduled_time` = ?", [nowTime]
    );

    if (rows.length > 0) {
        //  Get FCM token from DB
        let [FcmToken] = await connection.query(
            "SELECT `token` FROM `user_fcm_tokens`"
        );

        if (FcmToken.length > 0) {
            //
            FcmToken = FcmToken.map((el) => {
                el = el.token.toString();
                return el;
            });
            FcmToken = FcmToken.filter((fcm) => fcm !== "");

            await rows.forEach(async(n) => {
                let title = n.title;
                let message = n.message;
                let actionFor = n.scheduled_for;
                let passId = n.pass_id;

                while (FcmToken.length > 0) {
                    let NfcmToken = FcmToken.splice(0, 3);
                    // console.log("Sending scheduled notification on : ", NfcmToken);

                    await global.fastify.sendPushNotification(
                        title,
                        message,
                        NfcmToken,
                        actionFor,
                        passId
                    );
                }
            });
        }
    }

    connection.release();
    // console.log(response);
};

module.exports = {
    sendNotification,
};