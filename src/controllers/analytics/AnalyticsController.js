const payments = require("../../models/payments.model");

const getAnalytics = async (request, reply) => {
  // try {
  payments_data = await payments.findMany();
  // return the response here
  return reply.send({ data: { payments: payments_data } });
  // } catch (error) {
  //   return reply.code(422).send({ error: { ...error } });
  // }
};
const getAnalyticsById = async (request, reply) => {
  // try {
  payments_data = await payments.findMany({
    where: {
      charge_valid: {
        equals: true, // Default mode
      },
      passes: true,
      passes: {
        bar: {
          id: request.params.id,
        },
      },
    },
  });
  // return the response here
  return reply.send({ data: { analytics: payments_data } });
  // } catch (error) {
  //   return reply.code(422).send({ error: { ...error } });
  // }
};
const getAnalyticsByLocationId = async (request, reply) => {
  // try {
  payments_data = await payments.findMany({
    where: {
      charge_valid: {
        equals: true, // Default mode
      },
      passes: true,
      passes: {
        location: {
          id: request.params.id,
        },
      },
    },
  });
  // return the response here
  return reply.send({ data: { analytics: payments_data } });
  // } catch (error) {
  //   return reply.code(422).send({ error: { ...error } });
  // }
};

module.exports = {
  getAnalytics,
  getAnalyticsById,
  getAnalyticsByLocationId,
};
