const DateFormat = require("../../helper/DateFormat");
const otpHelper = require("../../helper/OtpGenerator");
const smsHelper = require("../../helper/SmsHelper");
const emailHelper = require("../../helper/EmailHelper");
const me = async (req, reply) => {
  try {
    reply.status(200).send({
      success: true,
      message: "User Fetched Successfully.",
      data: {
        user: req.user,
      },
    });
  } catch (error) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
    });
  }
};
// code for sending OTP to email :
const sendEmailOtp = async (req, reply) => {
  try {
    const contact = req.params.contact;
    if (contact !== "") {
      // DB Stuffs
      const connection = await global.fastify.mysql.getConnection();
      const [rows] = await connection.query(
        "SELECT `email` FROM `bar` where `email` = ?",
        [contact]
      );
      connection.release();

      if (rows.length > 0) {
        // Generate random OTP
        const otp = await otpHelper.generateOtp();

        // send OTP to user mobile
        const msg =
          "Hi There, <br/> Your One time code for Venue login is <b>" +
          otp +
          "</b>. <br/>Thank you.";
        const sendSuccess = await emailHelper.send(contact, msg); //await smsHelper.send(contact, msg);
        if (sendSuccess.accepted.length > 0) {
          // update IN DB
          const connection = await global.fastify.mysql.getConnection();
          await connection.query(
            "UPDATE `bar` SET `otp` = ? where `email` = ?",
            [otp, contact]
          );
          connection.release();

          reply.status(200).send({
            success: true,
            message: "Your Passcode was sent successfully to Email.",
          });
        } else {
          reply.status(422).send({
            success: false,
            message: "Oops! Unable to send Passcode.",
          });
        }
      } else {
        reply.status(422).send({
          success: false,
          message: "Email does not exists.",
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Email is required, Please send it as a parameter.",
      });
    }
  } catch (error) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
    });
  }
};

const sendOtp = async (req, reply) => {
  try {
    const contact = req.params.contact;
    if (contact !== "") {
      // DB Stuffs
      const connection = await global.fastify.mysql.getConnection();
      const [rows] = await connection.query(
        "SELECT `contact` FROM `admin` where `contact` = ?",
        [contact]
      );
      connection.release();

      if (rows.length > 0) {
        // Generate random OTP
        const otp = await otpHelper.generateOtp();

        // send OTP to user mobile
        const msg = "Your verification code: " + otp + ". Thank you.";
        const sendSuccess = await smsHelper.send(contact, msg);

        if (sendSuccess === true) {
          // update IN DB
          const connection = await global.fastify.mysql.getConnection();
          await connection.query(
            "UPDATE `admin` SET `otp` = ? where `contact` = ?",
            [otp, contact]
          );
          connection.release();

          reply.status(200).send({
            success: true,
            message: "OTP send successfully to admin contact.",
          });
        } else {
          reply.status(422).send({
            success: false,
            message: "Oops! Unable to send OTP.",
          });
        }
      } else {
        reply.status(422).send({
          success: false,
          message: "Contact does not exists.",
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Contact is required, Please send it as a parameter.",
      });
    }
  } catch (error) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
    });
  }
};
//venue-login
const venue_login = async (req, reply) => {
  try {
    // console.log(req.body)
    const { email, otp } = req.body;

    if (email !== "" && otp !== "") {
      // DB Operation
      const connection = await global.fastify.mysql.getConnection();
      const [rows] = await connection.query(
        "SELECT `id`,`location_id` FROM `bar` WHERE `email` = ? AND `otp` = ?",
        [email, otp]
      );

      if (rows.length > 0) {
        // generate token
        const token = global.fastify.jwt.sign({
          contact: email,
          role: "venue",
          associated_venue_ids: rows,
        });

        // Change OTP
        const otp = await otpHelper.generateOtp();
        await connection.query("UPDATE `bar` SET `otp` = ? where `email` = ?", [
          otp,
          email,
        ]);
        connection.release();

        reply.status(200).send({
          success: true,
          message: "Successful Venue Login.",
          data: {
            token,
            associated_venue_ids: rows,
          },
        });
      } else {
        connection.release();
        reply.status(422).send({
          success: false,
          message: "Oops! OTP not matched. Try again.",
          data: {},
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Email and OTP fields are required.",
        data: {},
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {},
    });
  }
};

const login = async (req, reply) => {
  try {
    // console.log(req.body)
    const { contact, otp } = req.body;

    if (contact !== "" && otp !== "") {
      // DB Operation
      const connection = await global.fastify.mysql.getConnection();
      const [rows] = await connection.query(
        "SELECT `id` FROM `admin` WHERE `contact` = ? AND `otp` = ?",
        [contact, otp]
      );

      if (rows.length > 0) {
        // generate token
        const token = global.fastify.jwt.sign({ contact, role: "admin" });

        // Change OTP
        const otp = await otpHelper.generateOtp();
        await connection.query(
          "UPDATE `admin` SET `otp` = ? where `contact` = ?",
          [otp, contact]
        );
        connection.release();

        reply.status(200).send({
          success: true,
          message: "Admin login success.",
          data: {
            token,
          },
        });
      } else {
        connection.release();
        reply.status(422).send({
          success: false,
          message: "Oops! OTP not matched. Try again.",
          data: {},
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Contact and OTP fields are required.",
        data: {},
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {},
    });
  }
};

const isLogin = async (req, reply) => {
  try {
    reply.status(200).send({
      success: true,
      message: "Authentication success. Redirecting...",
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Oops!",
      message: "Oops! Something went wrong.",
    });
  }
};

const getPasses = async (req, reply) => {
  try {
    // type are daily, upcoming, expired

    const status = "active";
    const currentDate = DateFormat.getDateFormatted(new Date());
    const currentDay = DateFormat.getDayFormatted(new Date());

    const passType =
      req.query.pass_type && req.query.pass_type !== ""
        ? req.query.pass_type
        : "daily";

    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    let totalPage = 0;
    const pageLimit = 10;

    const bar_id = req.query.bar_id ? req.query.bar_id : -1;
    const conditional_bar_id_query =
      bar_id === -1 ? "" : " AND bar_id=" + bar_id + " ";
    // Check if any filter's are applied
    if (passType && passType !== "") {
      pageLimitStart = (pageNo - 1) * pageLimit;
      const connection = await global.fastify.mysql.getConnection();
      if (passType === "daily") {
        const [rows] = await connection.query(
          'SELECT  count(`id`) as `pages` FROM `passes` WHERE `active_from` >= ? AND `valid_till` >= ? AND `type` =? AND `status` = ?  OR `id` IN (SELECT `id` FROM `passes` WHERE `type` = "' +
            passType +
            '" AND `is_recurring` = 1 AND `status` = "' +
            status +
            '")',
          [currentDate, currentDate, passType, status]
        );

        console.log(conditional_bar_id_query);
        const [passes] = await connection.query(
          'SELECT  * FROM `passes` WHERE `valid_till` >= ? AND `type` = ? AND `status` = ? OR `id` IN (SELECT `id` FROM `passes` WHERE `type` = "' +
            passType +
            '" ' +
            // '" AND `is_recurring` = 1  ' +
            conditional_bar_id_query +
            ' AND `status` = "' +
            status +
            '") ORDER BY `active_from` ASC, `id` DESC LIMIT ?, ?',
          [currentDate, passType, status, pageLimitStart, pageLimit]
        );
        // console.log([passes]);
        connection.release();

        // Calc pages
        totalPage = rows[0].pages;
        totalPage = Math.ceil(totalPage / pageLimit);

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            total_page: totalPage,
            pass_type: passType,
            passes,
          },
        });
      } else if (passType === "upcoming") {
        console.log("here ");
        const [rows] = await connection.query(
          "SELECT  count(`id`) as `pages` FROM `passes` WHERE `active_from` >= ? AND `valid_till` >= ? AND `status` = ? AND `type` =?",
          [currentDate, currentDate, status, passType]
        );
        const [passes] = await connection.query(
          //"SELECT  * FROM `passes` WHERE `valid_till` >= ? AND `status` = ? AND `type` = ? " +
          "SELECT  * FROM `passes` WHERE `status` = ? AND `type` = ? " +
            conditional_bar_id_query +
            "ORDER BY `active_from` ASC LIMIT ?, ?",
          [status, passType, pageLimitStart, pageLimit]
        );
        connection.release();

        // Calc pages
        totalPage = rows[0].pages;
        totalPage = Math.ceil(totalPage / pageLimit);

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            total_page: totalPage,
            pass_type: passType,
            passes,
          },
        });
      } else if (passType === "expired") {
        const [rows] = await connection.query(
          "SELECT  count(`id`) as `pages` FROM `passes` WHERE `valid_till` < ? AND `status` = ? AND `is_recurring` = 0",
          [currentDate, status]
        );
        const [passes] = await connection.query(
          "SELECT  * FROM `passes` WHERE `valid_till` < ? AND `status` = ? AND `is_recurring` = 0 " +
            conditional_bar_id_query +
            "ORDER BY `id` DESC LIMIT ?, ?",
          [currentDate, status, pageLimitStart, pageLimit]
        );
        connection.release();

        // Calc pages
        totalPage = rows[0].pages;
        totalPage = Math.ceil(totalPage / pageLimit);

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            total_page: totalPage,
            pass_type: passType,
            passes,
          },
        });
      } else if (passType === "block") {
        const bStatus = "block";
        const [rows] = await connection.query(
          "SELECT  count(`id`) as `pages` FROM `passes` WHERE `status` = ?",
          [bStatus]
        );
        const [passes] = await connection.query(
          "SELECT  * FROM `passes` WHERE `status` = ? " +
            conditional_bar_id_query +
            "ORDER BY `id` DESC LIMIT ?, ?",
          [bStatus, pageLimitStart, pageLimit]
        );
        connection.release();

        // Calc pages
        totalPage = rows[0].pages;
        totalPage = Math.ceil(totalPage / pageLimit);

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            total_page: totalPage,
            pass_type: passType,
            passes,
          },
        });
      } else if (passType === "line_hop") {
        const [rows] = await connection.query(
          "SELECT  count(`id`) as `pages` FROM `passes` WHERE `active_from` >= ? AND `valid_till` >= ? AND `status` = ? AND `type` =?",
          [currentDate, currentDate, status, passType]
        );
        const [passes] = await connection.query(
          "SELECT  * FROM `passes` WHERE `type` = ? AND `status` = ?" +
            conditional_bar_id_query +
            "ORDER BY `active_from` ASC LIMIT ?, ?",
          [passType, status, pageLimitStart, pageLimit]
        );
        connection.release();

        // Calc pages
        totalPage = rows[0].pages;
        totalPage = Math.ceil(totalPage / pageLimit);

        reply.status(200).send({
          success: true,
          message: "Passes fetched successfully. with filter options.",
          data: {
            page_no: pageNo,
            total_page: totalPage,
            pass_type: passType,
            passes,
          },
        });
      }
    } else {
      reply.status(401).send({
        success: false,
        error: "Required entity is missing.",
        message: "Pass type is required to get passes.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: error,
      message: "Oops! Something went wrong.",
    });
  }
};

const addPass = async (req, reply) => {
  try {
    const name = req.body.name;
    const tagline = req.body.tagline;
    const icon = req.body.icon;
    const banner = req.body.banner;
    const saving = req.body.saving;
    const activeFrom = req.body.active_from;
    const validTill = req.body.valid_till;
    const additionals = req.body.additionals;

    const isPlus = req.body.isPlus;
    const type = req.body.type;
    const activeDay = req.body.active_day;
    const isRecurring = req.body.is_recurring;

    const addressLine1 = req.body.address_line_1;
    const addressLine2 = req.body.address_line_2;
    const city = req.body.city;
    const state = req.body.state;
    const pinCode = req.body.pin_code;

    const latitude = req.body.latitude;
    const longitude = req.body.longitude;

    const bar_id = req.body.bar_id;
    const location_id = req.body.location_id;

    const connection = await global.fastify.mysql.getConnection();
    const [newPass] = await connection.query(
      "INSERT INTO `passes` (`name`, `tagline`, `icon`, `banner`, `saving`, `active_from`, `valid_till`, `additionals`, `isPlus`, `type`, `active_day`, `is_recurring`, `address_line_1`, `address_line_2`, `city`, `state`, `pin_code`, `latitude`, `longitude`,`bar_id`,`location_id`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)",
      [
        name,
        tagline,
        icon,
        banner,
        saving,
        activeFrom,
        validTill,
        additionals,
        isPlus,
        type,
        activeDay,
        isRecurring,
        addressLine1,
        addressLine2,
        city,
        state,
        pinCode,
        latitude,
        longitude,

        bar_id,
        location_id,
      ]
    );

    if (newPass.insertId > 0) {
      const passAttributes = req.body.pass_attributes;

      passAttributes.forEach(async (obj) => {
        if (obj.title !== "") {
          await connection.query(
            "INSERT INTO `pass_attributes`(`pass_id`, `title`) VALUES (?, ?)",
            [newPass.insertId, obj.title]
          );
        }
      });

      // Create schedule for notification
      // const newPassId = newPass.insertId;
      // const currentUser = "pass";
      // let scheduledFor = "pass_added";
      // // Type
      // if (type === "daily") {
      //   if (isRecurring && isRecurring == true) {
      //     // Get next recent date of active day
      //     // Schedule on same as active time
      //     const message = "New offer awaits for you... " + tagline;
      //     const activeTime = DateFormat.getDateOfDay(activeDay);
      //     scheduledFor = "pass_active";
      //     await connection.query(
      //       "INSERT INTO `scheduled_notifications`(`title`, `message`, `scheduled_time`, `scheduled_for`, `added_by`, `pass_id`) VALUES (?, ?, ?, ?, ?, ?)",
      //       [name, message, activeTime, scheduledFor, currentUser, newPassId]
      //     );
      //   } else {
      //     // Schedule on same as active time
      //     const message = "New offer awaits... " + tagline;
      //     scheduledFor = "pass_active";
      //     await connection.query(
      //       "INSERT INTO `scheduled_notifications`(`title`, `message`, `scheduled_time`, `scheduled_for`, `added_by`, `pass_id`) VALUES (?, ?, ?, ?, ?, ?)",
      //       [name, message, activeFrom, scheduledFor, currentUser, newPassId]
      //     );
      //   }
      // } else if (type === "upcoming") {
      //   // schedule on visible from
      //   let sMessage = "Exclusive pass added";
      //   let activeOn = activeFrom;
      //   activeOn = activeOn.split(" ")[0] + " 00:00:00";
      //   scheduledFor = "pass_added";
      //   if (tagline != "") {
      //     sMessage = sMessage + ", " + tagline;
      //   }
      //   await connection.query(
      //     "INSERT INTO `scheduled_notifications`(`title`, `message`, `scheduled_time`, `scheduled_for`, `added_by`, `pass_id`) VALUES (?, ?, ?, ?, ?, ?)",
      //     [name, sMessage, activeOn, scheduledFor, currentUser, newPassId]
      //   );

      //   // schedule on active time
      //   sMessage = "Exclusive pass is active now";
      //   scheduledFor = "pass_active";
      //   if (tagline != "") {
      //     sMessage = sMessage + ", " + tagline;
      //   }
      //   await connection.query(
      //     "INSERT INTO `scheduled_notifications`(`title`, `message`, `scheduled_time`, `scheduled_for`, `added_by`, `pass_id`) VALUES (?, ?, ?, ?, ?, ?)",
      //     [name, sMessage, activeFrom, scheduledFor, currentUser, newPassId]
      //   );
      // }

      connection.release();

      reply.status(200).send({
        success: true,
        message: "Pass added successfully.",
      });
    } else {
      connection.release();
      reply.status(401).send({
        success: false,
        error: "Oops!",
        message: "Unable to save pass data.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error,
      message: "Oops! Something went wrong.",
    });
  }
};

const updatePass = async (req, reply) => {
  try {
    const id = req.params.id;

    // check if pass id exists
    const connection = await global.fastify.mysql.getConnection();
    const [isExists] = await connection.query(
      "SELECT `id` FROM `passes` WHERE `id` = ?",
      [id]
    );

    if (isExists.length > 0) {
      // Update pass details
      const name = req.body.name;
      const tagline = req.body.tagline;
      const icon = req.body.icon;
      const banner = req.body.banner;
      const saving = req.body.saving;
      const activeFrom = req.body.active_from;
      const validTill = req.body.valid_till;
      const additionals = req.body.additionals;

      const isPlus = req.body.isPlus;

      const type = req.body.type;
      const activeDay = req.body.active_day;
      const isRecurring = req.body.is_recurring;

      const addressLine1 = req.body.address_line_1;
      const addressLine2 = req.body.address_line_2;
      const city = req.body.city;
      const state = req.body.state;
      const pinCode = req.body.pin_code;

      const latitude = req.body.latitude;
      const longitude = req.body.longitude;
      const status = req.body.status;

      // Update main details
      await connection.query(
        "UPDATE `passes` SET `name`= ?, `tagline`= ?, `icon`= ?, `banner`= ?, `saving`= ?, `active_from`= ?, `valid_till`= ?, `additionals`= ?, `isPlus`= ?, `type`= ?, `active_day`= ?, `is_recurring`= ?, `address_line_1`= ?, `address_line_2`= ?, `city`= ?, `state`= ?, `pin_code`= ?, `latitude`= ?, `longitude`= ?, `status`= ? WHERE `id` = ?",
        [
          name,
          tagline,
          icon,
          banner,
          saving,
          activeFrom,
          validTill,
          additionals,
          isPlus,
          type,
          activeDay,
          isRecurring,
          addressLine1,
          addressLine2,
          city,
          state,
          pinCode,
          latitude,
          longitude,
          status,
          id,
        ]
      );

      // Update attributes
      const passAttributes = req.body.pass_attributes;

      if (passAttributes.length > 0) {
        // Remove attributes
        await connection.query(
          "DELETE FROM `pass_attributes` WHERE `pass_id` = ?",
          [id]
        );

        passAttributes.forEach(async (obj) => {
          await connection.query(
            "INSERT INTO `pass_attributes`(`pass_id`, `title`) VALUES (?, ?)",
            [id, obj.title]
          );
        });
      }

      connection.release();

      reply.status(200).send({
        success: true,
        message: "Pass updated successfully.",
      });
    } else {
      connection.release();
      reply.status(401).send({
        success: false,
        error: "Oops!",
        message: "Oops! This pass does not exists in our database.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: error,
      message: "Oops! Something went wrong.",
    });
  }
};

const users = async (req, reply) => {
  try {
    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    const pageLimit = 10;
    let totalPage = 0;
    pageLimitStart = (pageNo - 1) * pageLimit;
    const connection = await global.fastify.mysql.getConnection();
    const [rows] = await connection.query(
      "SELECT count(`id`) as `pages` FROM `users`"
    );
    // const [users] = await connection.query(
    //   'SELECT * FROM `users` ORDER BY `id` DESC LIMIT ?, ?', [pageLimitStart, pageLimit]
    // )
    // const [users] = await connection.query(
    //   'SELECT `users`.`id`, `users`.`first_name`, `users`.`profile_image`, `users`.`contact`, `users`.`dob`, DATE_FORMAT(`users`.`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at` FROM `users` ORDER BY `id` DESC LIMIT ?, ?', [pageLimitStart, pageLimit]
    // )
    const [users] = await connection.query(
      'SELECT COUNT(`redeemed_passed`.`id`) as `redeemed_passes`, `users`.`id`, `users`.`first_name`, `users`.`profile_image`, `users`.`contact`, `users`.`dob`, `users`.`is_hop_plus`, `users`.`referred_by`, DATE_FORMAT(`users`.`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at` FROM `users` LEFT JOIN `redeemed_passed` ON `users`.`id` = `redeemed_passed`.`user_id` GROUP BY `users`.`id` ORDER BY `users`.`id` DESC LIMIT ?, ?',
      [pageLimitStart, pageLimit]
    );

    connection.release();

    // Calc pages
    totalPage = rows[0].pages;
    totalPage = Math.ceil(totalPage / pageLimit);

    reply.status(200).send({
      success: true,
      message: "User list fetched successfully.",
      data: {
        page_no: pageNo,
        total_page: totalPage,
        users,
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: error,
      message: "Oops! Something went wrong.",
    });
  }
};

const userDetails = async (req, reply) => {
  try {
    const id = req.params.id;
    if (id > 0) {
      const connection = await global.fastify.mysql.getConnection();
      // Get redeemed passes list
      const [redeemedPasses] = await connection.query(
        'SELECT `passes`.`id` as `pass_id`, `passes`.`name`, `passes`.`tagline`, `passes`.`icon`, `passes`.`banner`, `passes`.`saving`, DATE_FORMAT(`passes`.`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`passes`.`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `passes`.`additionals`, `passes`.`isPlus`, `passes`.`type`, `passes`.`active_day`, `passes`.`is_recurring`, `passes`.`address_line_1`, `passes`.`address_line_2`, `passes`.`city`, `passes`.`state`, `passes`.`pin_code`, `passes`.`latitude`, `passes`.`longitude`, `passes`.`status`, DATE_FORMAT(`redeemed_passed`.`redeemed_at`, "%Y-%m-%d %H:%i:%s") as `redeemed_at` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` WHERE `redeemed_passed`.`user_id` = ?',
        [id]
      );

      connection.release();

      reply.status(200).send({
        success: true,
        message: "User details fetched successfully.",
        data: {
          redeemed_passes: redeemedPasses,
        },
      });
    } else {
      reply.status(401).send({
        success: false,
        error: "Opps!",
        message: "User id is required as parameter.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Oops!",
      message: "Oops! Something went wrong.",
    });
  }
};

const addMedia = async (req, reply) => {
  try {
    const fileName = req.file.filename;
    const type = req.body.media_type;

    if (fileName !== "" && fileName !== null && type !== "") {
      const connection = await global.fastify.mysql.getConnection();
      // Get redeemed passes list
      await connection.query(
        "INSERT INTO `media`(`name`, `type`) VALUES (?, ?)",
        [fileName, type]
      );

      connection.release();

      reply.status(200).send({
        success: true,
        message: "New " + type + " added successfully.",
      });
    } else {
      reply.status(401).send({
        success: false,
        error: "Opps!",
        message: "Select file and type, both are required.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Oops!",
      message: "Oops! Something went wrong. Error: " + error,
    });
  }
};

const getMedia = async (req, reply) => {
  try {
    const mediaType = req.query.media_type;

    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    let totalPage = 0;
    const pageLimit = 10;

    pageLimitStart = (pageNo - 1) * pageLimit;

    // Check if any filter's are applied
    if (mediaType && mediaType !== "") {
      const connection = await global.fastify.mysql.getConnection();

      const [media] = await connection.query(
        'SELECT `id`, `name`, `type`, DATE_FORMAT(`added_date`, "%Y-%m-%d %H:%i:%s") as `added_date` FROM `media` WHERE type = ? ORDER BY `id` DESC LIMIT ?, ?',
        [mediaType, pageLimitStart, pageLimit]
      );

      const [rows] = await connection.query(
        "SELECT count(`id`) as `pages` FROM `media` WHERE type = ?",
        [mediaType]
      );

      // Calc pages
      totalPage = rows[0].pages;
      totalPage = Math.ceil(totalPage / pageLimit);

      connection.release();

      reply.status(200).send({
        success: true,
        message: "Media fetched successfully. with filter options.",
        data: {
          page_no: pageNo,
          total_page: totalPage,
          media_type: mediaType,
          media,
        },
      });
    } else {
      // If there is no any filter applied / Send both data
      const connection = await global.fastify.mysql.getConnection();
      const [media] = await connection.query(
        'SELECT `id`, `name`, `type`, DATE_FORMAT(`added_date`, "%Y-%m-%d %H:%i:%s") as `added_date` FROM `media` ORDER BY `id` DESC LIMIT ?, ?',
        [pageLimitStart, pageLimit]
      );

      const [rows] = await connection.query(
        "SELECT count(`id`) as `pages` FROM `media`"
      );

      // Calc pages
      totalPage = rows[0].pages;
      totalPage = Math.ceil(totalPage / pageLimit);

      connection.release();

      reply.status(200).send({
        success: true,
        message: "Media fetched successfully.",
        data: {
          page_no: pageNo,
          total_page: totalPage,
          media,
        },
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Oops!",
      message: "Oops! Something went wrong. Error: " + error,
    });
  }
};

const removeMedia = async (req, reply) => {
  try {
    const id = req.params.id;
    const url = req.body.url;

    const connection = await global.fastify.mysql.getConnection();

    const [mediaRow] = await connection.query(
      "SELECT `id`, `name`, `icon`, `banner` FROM `passes` WHERE `icon` = ? OR `banner` = ?",
      [url, url]
    );

    if (mediaRow.length > 0) {
      connection.release();
      reply.status(401).send({
        success: false,
        message:
          "This media is being used with the pass(s). We can't remove it. You can only remove passes that are not being used with a pass.",
        data: {},
      });
    } else {
      // get filename from DB
      const [fileName] = await connection.query(
        "SELECT `name` FROM `media` WHERE `id` =?",
        [id]
      );
      const fileNameToRemove = fileName[0].name;

      // Get Full Path
      const fileToRemove = require("path").resolve(
        __dirname,
        "../../../media/" + fileNameToRemove
      );

      // Remove media file
      // include node fs module
      const fs = require("fs");

      // delete file
      // let isDeleted = true
      fs.unlinkSync(fileToRemove, async function (err) {
        if (!err) {
          console.log("FILE DELETED");
        }
      });

      // remove media from DB
      await connection.query("DELETE FROM `media` WHERE `id` = ?", [id]);
      connection.release();

      reply.status(200).send({
        success: true,
        message: "Media removed successfully.",
        data: {},
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error,
      message: "Oops! Something went wrong.",
    });
  }
};

const sendPushNotification = async (req, reply) => {
  try {
    const { title, message, location_id } = req.body;

    let FcmToken;
    // Get FCM token from DB
    const connection = await global.fastify.mysql.getConnection();
    if (location_id != 0) {
      [FcmToken] = await connection.query(
        "SELECT user_fcm_tokens.`token` FROM `user_fcm_tokens` , users WHERE user_fcm_tokens.user_id=users.id AND users.location_id=" +
          location_id
      );
    } else {
      [FcmToken] = await connection.query(
        "SELECT `token` FROM `user_fcm_tokens`"
      );
    }

    if (FcmToken.length > 0) {
      //
      FcmToken = FcmToken.map((el) => {
        el = el.token.toString();
        return el;
      });

      FcmToken = FcmToken.filter((fcm) => fcm !== "");
      const actionFor = "general_push_notification";

      while (FcmToken.length > 0) {
        let NfcmToken = FcmToken.splice(0, 999);
        // console.log("Sending on : ", NfcmToken);
        global.fastify.sendPushNotification(
          title,
          message,
          NfcmToken,
          actionFor
        );
      }

      reply.status(200).send({
        success: true,
        message: "Push notification send successfully.",
        data: { fcm: [FcmToken] },
      });
    } else {
      reply.status(200).send({
        success: true,
        message: "No user found for notification.",
        data: {},
      });
    }
  } catch (error) {
    console.log(error);
    reply.status(401).send({
      success: false,
      message: "Oops! Something went wrong.",
      error,
    });
  }
};

const schedulePushNotification = async (req, reply) => {
  try {
    const { title, message, scheduled_time, location_id } = req.body;
    const currentUser = "admin";
    // Get FCM token from DB
    const connection = await global.fastify.mysql.getConnection();
    const [currentSchedule] = await connection.query(
      "INSERT INTO `scheduled_notifications`(`title`, `message`, `scheduled_time`, `added_by`) VALUES (?, ?, ?, ?)",
      [title, message, scheduled_time, currentUser]
    );

    connection.release();

    reply.status(200).send({
      success: true,
      message: "Push notification scheduled successfully.",
      data: {},
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! Something went wrong.",
      error,
    });
  }
};

const getSchedulePushNotification = async (req, reply) => {
  try {
    const currentDate = DateFormat.getDateMinuteFormatted(new Date());

    const type =
      req.query.type && req.query.type !== "" ? req.query.type : "active";

    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    let totalPage = 0;
    const pageLimit = 10;

    let scheduled_messages = [];

    pageLimitStart = (pageNo - 1) * pageLimit;
    const connection = await global.fastify.mysql.getConnection();

    // Get Data of active
    if (type === "active") {
      const [rows] = await connection.query(
        "SELECT  count(`id`) as `pages` FROM `scheduled_notifications` WHERE `scheduled_time` >= ?",
        [currentDate]
      );
      scheduled_messages = await connection.query(
        "SELECT `id`, `title`, `message`, DATE_FORMAT(`scheduled_time`, '%Y-%m-%d %H:%i:%s') as `scheduled_time`, `added_by`, `pass_id`, `status`, `created_at` FROM `scheduled_notifications` WHERE `scheduled_time` >= ? ORDER BY `scheduled_time` ASC, `id` DESC LIMIT ?, ?",
        [currentDate, pageLimitStart, pageLimit]
      );

      // Calc pages
      totalPage = rows[0].pages;
      totalPage = Math.ceil(totalPage / pageLimit);
    }

    // Get data of admin or pass
    else if (type === "admin" || type === "pass") {
      const [rows] = await connection.query(
        "SELECT  count(`id`) as `pages` FROM `scheduled_notifications` WHERE `scheduled_time` >= ? AND `added_by` = ?",
        [currentDate, type]
      );
      scheduled_messages = await connection.query(
        "SELECT `id`, `title`, `message`, DATE_FORMAT(`scheduled_time`, '%Y-%m-%d %H:%i:%s') as `scheduled_time`, `added_by`, `pass_id`, `status`, `created_at` FROM `scheduled_notifications` WHERE `scheduled_time` >= ? AND `added_by` = ? ORDER BY `scheduled_time` ASC, `id` DESC LIMIT ?, ?",
        [currentDate, type, pageLimitStart, pageLimit]
      );

      // Calc pages
      totalPage = rows[0].pages;
      totalPage = Math.ceil(totalPage / pageLimit);
    }

    // Get data of expired
    else if (type === "expired") {
      const [rows] = await connection.query(
        "SELECT  count(`id`) as `pages` FROM `scheduled_notifications` WHERE `scheduled_time` < ?",
        [currentDate]
      );
      scheduled_messages = await connection.query(
        "SELECT `id`, `title`, `message`, DATE_FORMAT(`scheduled_time`, '%Y-%m-%d %H:%i:%s') as `scheduled_time`, `added_by`, `pass_id`, `status`, `created_at` FROM `scheduled_notifications` WHERE `scheduled_time` < ? ORDER BY `scheduled_time` ASC, `id` DESC LIMIT ?, ?",
        [currentDate, pageLimitStart, pageLimit]
      );

      // Calc pages
      totalPage = rows[0].pages;
      totalPage = Math.ceil(totalPage / pageLimit);
    }

    connection.release();

    // Send response
    reply.status(200).send({
      success: true,
      message: "Scheduled messages fetched successfully. with filter options.",
      currentDate,
      data: {
        page_no: pageNo,
        total_page: totalPage,
        type: type,
        scheduled_messages: scheduled_messages[0],
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Error in fetching scheduled messages." + error,
      data: {},
    });
  }
};

const deleteSchedulePushNotification = async (req, reply) => {
  try {
    const { id } = req.body;

    const connection = await global.fastify.mysql.getConnection();

    await connection.query(
      "DELETE FROM `scheduled_notifications` WHERE `id` = ?",
      [id]
    );

    connection.release();

    reply.status(200).send({
      success: true,
      message: "Removed successfully.",
      data: { id },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
      data: {},
    });
  }
};

const createNewPaidUser = async (req, reply) => {
  try {
    const { name, contact } = req.body;

    const connection = await global.fastify.mysql.getConnection();

    const [isExists] = await connection.query(
      "SELECT `id` FROM `paid_user_via_admin` WHERE `contact` = ?",
      [contact]
    );

    if (isExists.length > 0) {
      connection.release();

      reply.status(200).send({
        success: true,
        message: "User's contact already exists.",
        data: {},
      });
    } else {
      // Create an user

      await connection.query(
        "INSERT INTO `paid_user_via_admin`( `name`, `contact`) VALUES (?,?)",
        [name, contact]
      );

      connection.release();
      reply.status(200).send({
        success: true,
        message: "User added successfully.",
        data: {},
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
      data: {},
    });
  }
};

const selectPaidUserViaUser = async (req, reply) => {
  try {
    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    let totalPage = 0;
    const pageLimit = 10;

    pageLimitStart = (pageNo - 1) * pageLimit;
    const connection = await global.fastify.mysql.getConnection();

    const [rows] = await connection.query(
      "SELECT  count(`id`) as `pages` FROM `paid_user_via_admin`"
    );

    // const [users] = await connection.query(
    //   "SELECT `id`, `name`, `contact`, `created_by`, `status`, `referral`, `is_ambassador`, DATE_FORMAT(`created_at`, '%Y-%m-%d %H:%i:%s') as `created_at` FROM `paid_user_via_admin` ORDER BY `id` DESC LIMIT ?, ?",
    //   [pageLimitStart, pageLimit]
    // );

    const [users] = await connection.query(
      "SELECT `paid_user_via_admin`.`id`, `paid_user_via_admin`.`name`, `paid_user_via_admin`.`contact`, `paid_user_via_admin`.`created_by`, `paid_user_via_admin`.`status`, `paid_user_via_admin`.`referral`, `paid_user_via_admin`.`is_ambassador`, DATE_FORMAT(`paid_user_via_admin`.`created_at`, '%Y-%m-%d %H:%i:%s') as `created_at`,  GROUP_CONCAT(`ambassador_referrals`.`referral`) as `more_referrals` FROM `paid_user_via_admin` LEFT JOIN `ambassador_referrals` ON `paid_user_via_admin`.`id` = `ambassador_referrals`.`user_id`  GROUP BY `paid_user_via_admin`.`id` ORDER BY `paid_user_via_admin`.`id` DESC LIMIT ?, ?",
      [pageLimitStart, pageLimit]
    );

    // Calc pages
    totalPage = rows[0].pages;
    totalPage = Math.ceil(totalPage / pageLimit);

    connection.release();

    reply.status(200).send({
      success: true,
      message: "User list fetched successfully.",
      data: {
        page_no: pageNo,
        total_page: totalPage,
        users,
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
      data: {},
    });
  }
};

const deleteNewPaidUser = async (req, reply) => {
  try {
    const { id } = req.body;

    const connection = await global.fastify.mysql.getConnection();

    await connection.query("DELETE FROM `paid_user_via_admin` WHERE `id` = ?", [
      id,
    ]);

    connection.release();

    reply.status(200).send({
      success: true,
      message: "Paid user removed successfully.",
      data: { id },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
      data: {},
    });
  }
};

const getSwipesCount = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    const today = DateFormat.getDateFormatted(new Date());

    const todayStart = today.split(" ")[0] + " 00:00:00";
    const todayEnd = today.split(" ")[0] + " 23:59:59";

    const weekStart =
      DateFormat.getDateFormattedDaysBefore(Date.now(), 7).split(" ")[0] +
      " 00:00:00";
    const monthStart =
      DateFormat.getDateFormattedDaysBefore(Date.now(), 30).split(" ")[0] +
      " 00:00:00";

    const [daily] = await connection.query(
      "SELECT COUNT(`id`) as `count` FROM `redeemed_passed` WHERE `redeemed_passed`.`redeemed_at` BETWEEN ? AND ?",
      [todayStart, todayEnd]
    );

    const [weekly] = await connection.query(
      "SELECT COUNT(`id`) as `count` FROM `redeemed_passed` WHERE `redeemed_passed`.`redeemed_at` BETWEEN ? AND ?",
      [weekStart, todayEnd]
    );

    const [monthly] = await connection.query(
      "SELECT COUNT(`id`) as `count` FROM `redeemed_passed` WHERE `redeemed_passed`.`redeemed_at` BETWEEN ? AND ?",
      [monthStart, todayEnd]
    );

    connection.release();

    reply.status(200).send({
      success: true,
      message: "Count get successfully.",
      data: {
        daily: daily[0].count,
        weekly: weekly[0].count,
        monthly: monthly[0].count,
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

const getUsersCount = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    let rc = 0;
    let pc = 0;
    let bc = 0;

    let paidStatus = 0;
    const [ru] = await connection.query(
      "SELECT COUNT(`id`) as `n` FROM `users` WHERE `is_hop_plus` = ?",
      [paidStatus]
    );
    rc = ru[0].n;

    paidStatus = 1;
    const [pu] = await connection.query(
      "SELECT COUNT(`id`) as `n` FROM `users` WHERE `is_hop_plus` = " +
        paidStatus +
        " AND `contact` NOT IN (SELECT `contact` FROM `paid_user_via_admin`)"
    );
    pc = pu[0].n;

    const [bu] = await connection.query(
      "SELECT COUNT(`id`) as `n` FROM `users` WHERE `is_hop_plus` = " +
        paidStatus +
        " AND `contact` IN (SELECT `contact` FROM `paid_user_via_admin`)"
    );
    bc = bu[0].n;

    connection.release();

    reply.status(200).send({
      success: true,
      message: "Count get successfully.",
      data: {
        series: [rc, pc, bc],
        labels: [
          `Regular User (${rc})`,
          `Paid User (${pc})`,
          `Brand Ambassador (${bc})`,
        ],
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

// const getRecurringSwipesCountBcup = async (req, reply) => {
//   try {
//     const connection = await global.fastify.mysql.getConnection();

//     let sun = (mon = tue = wed = thu = fri = sat = 0);

//     let d = "Sunday";
//     const [s] = await connection.query(
//       "SELECT COUNT(`id`) AS `n` FROM `redeemed_passed` WHERE `pass_id` IN (SELECT `id` FROM `passes` WHERE `is_recurring` = 1 AND `active_day` = '" +
//         d +
//         "')"
//     );
//     sun = s[0].n;

//     d = "Monday";
//     const [m] = await connection.query(
//       "SELECT COUNT(`id`) AS `n` FROM `redeemed_passed` WHERE `pass_id` IN (SELECT `id` FROM `passes` WHERE `is_recurring` = 1 AND `active_day` = '" +
//         d +
//         "')"
//     );
//     mon = m[0].n;

//     d = "Tuesday";
//     const [t] = await connection.query(
//       "SELECT COUNT(`id`) AS `n` FROM `redeemed_passed` WHERE `pass_id` IN (SELECT `id` FROM `passes` WHERE `is_recurring` = 1 AND `active_day` = '" +
//         d +
//         "')"
//     );
//     tue = t[0].n;

//     d = "Wednesday";
//     const [w] = await connection.query(
//       "SELECT COUNT(`id`) AS `n` FROM `redeemed_passed` WHERE `pass_id` IN (SELECT `id` FROM `passes` WHERE `is_recurring` = 1 AND `active_day` = '" +
//         d +
//         "')"
//     );
//     wed = w[0].n;

//     d = "Thursday";
//     const [th] = await connection.query(
//       "SELECT COUNT(`id`) AS `n` FROM `redeemed_passed` WHERE `pass_id` IN (SELECT `id` FROM `passes` WHERE `is_recurring` = 1 AND `active_day` = '" +
//         d +
//         "')"
//     );
//     thu = th[0].n;

//     d = "Friday";
//     const [f] = await connection.query(
//       "SELECT COUNT(`id`) AS `n` FROM `redeemed_passed` WHERE `pass_id` IN (SELECT `id` FROM `passes` WHERE `is_recurring` = 1 AND `active_day` = '" +
//         d +
//         "')"
//     );
//     fri = f[0].n;

//     d = "Saturday";
//     const [st] = await connection.query(
//       "SELECT COUNT(`id`) AS `n` FROM `redeemed_passed` WHERE `pass_id` IN (SELECT `id` FROM `passes` WHERE `is_recurring` = 1 AND `active_day` = '" +
//         d +
//         "')"
//     );
//     sat = st[0].n;

//     connection.release();

//     reply.status(200).send({
//       success: true,
//       message: "Count get successfully.",
//       data: {
//         series: [sun, mon, tue, wed, thu, fri, sat],
//         categories: [
//           "Sunday",
//           "Monday",
//           "Tuesday",
//           "Wednesday",
//           "Thursday",
//           "Friday",
//           "Saturday",
//         ],
//       },
//     });
//   } catch (error) {
//     reply.status(401).send({
//       success: false,
//       message: "Oops! " + error,
//     });
//   }
// };

const getRecurringSwipesCount = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    // SELECT count(`passes`.`id`) as `pages`, `passes`.`name` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` GROUP BY `passes`.`id`

    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    let totalPage = 0;
    let pp = req.query.per_page ? req.query.per_page : 10;
    pp = parseInt(pp);
    const pageLimit = pp;

    pageLimitStart = (pageNo - 1) * parseInt(pageLimit);

    const [rows] = await connection.query(
      "SELECT count(`passes`.`id`) as `pages` FROM `passes`"
    );
    const pType = "upcoming";
    const [passes] = await connection.query(
      "SELECT count(`redeemed_passed`.`id`) as `swipes`, `passes`.`name` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` WHERE `passes`.`type` = ? GROUP BY `passes`.`id` ORDER BY `passes`.`id` DESC LIMIT ?, ?",
      [pType, pageLimitStart, pageLimit]
    );
    connection.release();

    const series = [];
    const categories = [];

    passes.forEach((i) => {
      series.push(i.swipes);
      categories.push(i.name);
    });

    // Calc pages
    totalPage = rows[0].pages;
    totalPage = Math.ceil(totalPage / pageLimit);
    reply.status(200).send({
      success: true,
      message: "Count get successfully.",
      data: {
        series,
        categories,
        passes,
        totalPage,
        pageNo,
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

const getSwipesCountPerPass = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    // SELECT count(`passes`.`id`) as `pages`, `passes`.`name` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` GROUP BY `passes`.`id`

    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    let totalPage = 0;
    let pp = req.query.per_page ? req.query.per_page : 10;
    pp = parseInt(pp);
    const pageLimit = pp;

    pageLimitStart = (pageNo - 1) * parseInt(pageLimit);

    const [rows] = await connection.query(
      "SELECT count(`passes`.`id`) as `pages` FROM `passes`"
    );
    const [passes] = await connection.query(
      "SELECT count(`redeemed_passed`.`id`) as `swipes`, `passes`.`name` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` GROUP BY `passes`.`id` ORDER BY `passes`.`id` DESC LIMIT ?, ?",
      [pageLimitStart, pageLimit]
    );
    connection.release();

    const series = [];
    const categories = [];

    passes.forEach((i) => {
      series.push(i.swipes);
      categories.push(i.name);
    });

    // Calc pages
    totalPage = rows[0].pages;
    totalPage = Math.ceil(totalPage / pageLimit);
    reply.status(200).send({
      success: true,
      message: "Count get successfully.",
      data: {
        series,
        categories,
        passes,
        totalPage,
        pageNo,
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

const updateRefCodeBCUP = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    const { id, referral } = req.body;

    // Check referral does not exists
    const [isExists] = await connection.query(
      "SELECT `id` FROM `paid_user_via_admin` WHERE `referral` = ?",
      [referral]
    );

    if (isExists.length < 1) {
      // Update in DB
      await connection.query(
        "UPDATE `paid_user_via_admin` SET `referral`= ? WHERE `id` = ?",
        [referral, id]
      );

      connection.release();
      reply.status(200).send({
        success: true,
        message: "Referral code updated successfully.",
        data: { referral },
      });
    } else {
      connection.release();
      reply.status(200).send({
        success: false,
        message: "Sorry! This referral code is already exists.",
        data: { referral },
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

const updateRefCode = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    const { id, referral } = req.body;

    // Check referral does not exists
    const [isExistsInUser] = await connection.query(
      "SELECT `id` FROM `paid_user_via_admin` WHERE `referral` = ?",
      [referral]
    );
    const [isExistsInMore] = await connection.query(
      "SELECT `id` FROM `ambassador_referrals` WHERE `referral` = ?",
      [referral]
    );

    if (isExistsInUser.length > 0 || isExistsInMore.length > 0) {
      connection.release();
      reply.status(200).send({
        success: false,
        message: "Sorry! This referral code is already exists.",
        data: { referral },
      });
    } else {
      // Check if paid user referral column is empty
      const [isExistsInUserRef] = await connection.query(
        "SELECT `referral` FROM `paid_user_via_admin` WHERE `id` = ?",
        [id]
      );

      if (
        isExistsInUserRef[0].referral &&
        isExistsInUserRef[0].referral.length > 0
      ) {
        // Insert in DB
        await connection.query(
          "INSERT INTO `ambassador_referrals`(`user_id`, `referral`) VALUES (?,?)",
          [id, referral]
        );

        connection.release();

        reply.status(200).send({
          success: true,
          message: "Addon Referral code added successfully.",
          data: { isExistsInUserRef },
        });
      } else {
        // Update in DB
        await connection.query(
          "UPDATE `paid_user_via_admin` SET `referral`= ? WHERE `id` = ?",
          [referral, id]
        );

        connection.release();
        reply.status(200).send({
          success: true,
          message: "Referral code added successfully.",
          data: { isExistsInUserRef },
        });
      }
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

const getReferrals = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    const referral = req.query.referral;

    let refs = "'" + referral + "'";

    // Get more referrals
    const [moreRefs] = await connection.query(
      "SELECT * FROM `ambassador_referrals` WHERE `user_id` = (SELECT `id` FROM `paid_user_via_admin` WHERE `referral` = ?)",
      [referral]
    );

    if (moreRefs.length > 0) {
      moreRefs.forEach((mr) => {
        refs += ",'" + mr.referral + "'";
      });
    }

    // Check referral does not exists
    const [referrals] = await connection.query(
      "SELECT `id`, `first_name`, `contact`, `dob`, `is_hop_plus`, `referred_by`, `refer_is_paid`, DATE_FORMAT(`created_at`, '%Y-%m-%d %H:%i:%s') as  `created_at` FROM `users` WHERE `referred_by` IN (" +
        refs +
        ") ORDER BY `is_hop_plus` DESC, `id` DESC"
    );

    connection.release();
    reply.status(200).send({
      success: true,
      message: "Referrals fetched successfully.",
      data: { referrals },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

const markAsPaid = async (req, reply) => {
  try {
    const connection = await global.fastify.mysql.getConnection();

    const { id } = req.body;

    // Check referral does not exists
    await connection.query(
      "UPDATE `users` SET `refer_is_paid` = 'Paid' WHERE `users`.`id` = ?",
      [id]
    );

    connection.release();
    reply.status(200).send({
      success: true,
      message: "Marked as paid.",
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      message: "Oops! " + error,
    });
  }
};

// All Locations
const allLocations = async (req, reply) => {
  console.log("grabbing all locations");
  const table_name = "`location`";
  try {
    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    const pageLimit = 10;
    let totalPage = 0;
    pageLimitStart = (pageNo - 1) * pageLimit;
    const connection = await global.fastify.mysql.getConnection();
    const [rows] = await connection.query(
      "SELECT count(`id`) as `pages` FROM " + table_name
    );
    let locations;

    if (req.user.role === "venue") {
      const location_ids = [
        ...new Set(req.user.associated_venue_ids.map((a) => a.location_id)),
      ];

      [locations] = await connection.query(
        "SELECT * from " +
          table_name +
          " where id IN (" +
          location_ids +
          ") order by created_at DESC LIMIT ?, ?",
        [pageLimitStart, pageLimit]
      );
    } else {
      [locations] = await connection.query(
        "SELECT * from " + table_name + " order by created_at DESC LIMIT ?, ?",
        [pageLimitStart, pageLimit]
      );
    }

    connection.release();

    // Calc pages
    totalPage = rows[0].pages;
    totalPage = Math.ceil(totalPage / pageLimit);

    reply.status(200).send({
      success: true,
      message: "List fetched successfully.",
      data: {
        page_no: pageNo,
        total_page: totalPage,
        locations,
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: error,
      message: "Oops! Something went wrong.",
    });
  }
};

// All Bars
const allBars = async (req, reply) => {
  const table_name = "`bar`";
  try {
    const pageNo = req.query.page_no ? req.query.page_no : 1;
    let pageLimitStart = 0;
    const pageLimit = 10;
    let totalPage = 0;
    pageLimitStart = (pageNo - 1) * pageLimit;
    const connection = await global.fastify.mysql.getConnection();
    const [rows] = await connection.query(
      "SELECT count(`id`) as `pages` FROM " + table_name
    );

    const [bars] = await connection.query(
      "SELECT * from " + table_name + " order by created_at DESC LIMIT ?, ?",
      [pageLimitStart, pageLimit]
    );

    connection.release();

    // Calc pages
    totalPage = rows[0].pages;
    totalPage = Math.ceil(totalPage / pageLimit);

    reply.status(200).send({
      success: true,
      message: "List fetched successfully.",
      data: {
        page_no: pageNo,
        total_page: totalPage,
        bars,
      },
    });
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: error,
      message: "Oops! Something went wrong.",
    });
  }
};

module.exports = {
  me,
  sendEmailOtp,
  sendOtp,
  login,
  venue_login,
  isLogin,
  getPasses,
  addPass,
  updatePass,
  users,
  userDetails,
  addMedia,
  getMedia,
  removeMedia,
  sendPushNotification,
  schedulePushNotification,
  getSchedulePushNotification,
  deleteSchedulePushNotification,
  createNewPaidUser,
  selectPaidUserViaUser,
  deleteNewPaidUser,
  getSwipesCount,
  getUsersCount,
  getRecurringSwipesCount,
  getSwipesCountPerPass,
  updateRefCode,
  getReferrals,
  markAsPaid,
  // all-locations
  allLocations,
  allBars,
};
