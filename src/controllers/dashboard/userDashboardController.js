const decoder = require("../../helper/AuthTokenDecoder");

const profile = async (req, reply) => {
  // Decode token
  const authContact = await decoder.contact(req.headers.authorization);

  if (authContact !== "") {
    // Select user full details.
    const connection = await global.fastify.mysql.getConnection();
    const [rows] = await connection.query(
      "SELECT * FROM `users` where `contact` = ?",
      [authContact]
    );

    const user = rows[0];

    // Calc money saved
    const [money] = await connection.query(
      "SELECT `passes`.`saving`, `passes`.`id` as `pass_id` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` WHERE `redeemed_passed`.`user_id` = ?",
      [user.id]
    );

    const [isPaid] = await connection.query(
      "SELECT `id` FROM `paid_user_via_admin` where `contact` = ?",
      [authContact]
    );
    const [location] = await connection.query(
      "SELECT * FROM `location` WHERE `id` = ?",
      [rows[0].location_id]
    );
    user.location = location[0] || {};
    user.location.active = user.location.active == 1;
    connection.release();

    user.money_saved = money;
    user.is_paid = isPaid.length;

    reply.status(200).send({
      success: true,
      message: "User profile details fetched.",
      data: user,
    });
  } else {
    reply.status(401).send({
      success: false,
      error: "Unauthorized",
      message: "Authorization token is missing or invalid.",
    });
  }
};

const getRedeemedPasses = async (req, reply) => {
  // Decode token
  const authContact = await decoder.contact(req.headers.authorization);

  if (authContact !== "") {
    // Select user full details.
    const connection = await global.fastify.mysql.getConnection();
    const [rows] = await connection.query(
      "SELECT `id` FROM `users` where `contact` = ?",
      [authContact]
    );

    const user = rows[0];

    // Get redeemed passes list
    const [redeemedPasses] = await connection.query(
      'SELECT `passes`.`id` as `pass_id`, `passes`.`name`, `passes`.`tagline`, `passes`.`icon`, `passes`.`banner`, `passes`.`saving`, DATE_FORMAT(`passes`.`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`passes`.`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `passes`.`additionals`, `passes`.`isPlus`, `passes`.`type`, `passes`.`active_day`, `passes`.`is_recurring`, `passes`.`address_line_1`, `passes`.`address_line_2`, `passes`.`city`, `passes`.`state`, `passes`.`pin_code`, `passes`.`latitude`, `passes`.`longitude`, `passes`.`status`, DATE_FORMAT(`redeemed_passed`.`redeemed_at`, "%Y-%m-%d %H:%i:%s") as `redeemed_at` FROM `passes` LEFT JOIN `redeemed_passed` ON `passes`.`id` = `redeemed_passed`.`pass_id` WHERE `redeemed_passed`.`user_id` = ?',
      [user.id]
    );

    connection.release();

    reply.status(200).send({
      success: true,
      message: "Passes list fetched successfully.",
      data: redeemedPasses,
    });
  } else {
    reply.status(401).send({
      success: false,
      error: "Unauthorized",
      message: "Authorization token is missing or invalid.",
    });
  }
};

const redeemPass = async (req, reply) => {
  try {
    // Decode token
    const authContact = await decoder.contact(req.headers.authorization);

    if (authContact !== "") {
      // Get pass id
      const passId = req.body.pass_id;

      if (passId && passId > 0) {
        const connection = await global.fastify.mysql.getConnection();

        const [rows] = await connection.query(
          "SELECT `id` FROM `users` where `contact` = ?",
          [authContact]
        );

        const userId = rows[0].id;

        // Redeem pass
        const [isRedeemed] = await connection.query(
          "INSERT INTO `redeemed_passed`(`user_id`, `pass_id`) VALUES (?, ?)",
          [userId, passId]
        );

        if (isRedeemed.insertId > 0) {
          // Get Redeemed pass full detail
          const [passes] = await connection.query(
            'SELECT  `id`, `name`, `tagline`, `icon`, `banner`, `saving`, DATE_FORMAT(`active_from`, "%Y-%m-%d %H:%i:%s") as `active_from`, DATE_FORMAT(`valid_till`, "%Y-%m-%d %H:%i:%s") as `valid_till`, `additionals`, `isPlus`, `type`, `active_day`, `is_recurring`, `address_line_1`, `address_line_2`, `city`, `state`, `pin_code`, `latitude`, `longitude`, `status`, DATE_FORMAT(`created_at`, "%Y-%m-%d %H:%i:%s") as `created_at`, DATE_FORMAT(`updated_at`, "%Y-%m-%d %H:%i:%s") as `updated_at` FROM `passes` WHERE `id` = ?',
            [passId]
          );
          const [attr] = await connection.query(
            "SELECT `title` FROM `pass_attributes` WHERE `pass_id` = ?",
            [passId]
          );
          connection.release();
          const data = passes[0];
          data.attributes = attr;
          reply.status(200).send({
            success: true,
            message: "Pass redeemed successfully.",
            data,
          });
        } else {
          connection.release();

          reply.status(422).send({
            success: false,
            error: "Unprocessable Entity",
            message: "Oops! Something went wrong. Unable to redeem token.",
          });
        }
      } else {
        reply.status(422).send({
          success: false,
          error: "Unprocessable Entity",
          message: "Invalid pass id. Please enter correct pass id.",
        });
      }
    } else {
      reply.status(401).send({
        success: false,
        error: "Unauthorized",
        message: "Authorization token is missing or invalid.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Unauthorized",
      message: "Authorization token is missing or invalid.",
    });
  }
};

const updatePushNotification = async (req, reply) => {
  try {
    // Decode token
    const authContact = await decoder.contact(req.headers.authorization);

    if (authContact !== "") {
      // Get pass id
      const status = req.body.status;

      if (status !== null && status !== "") {
        const connection = await global.fastify.mysql.getConnection();

        const [rows] = await connection.query(
          "UPDATE `users` SET `push_notification` = ? where `contact` = ?",
          [status, authContact]
        );
        connection.release();

        reply.status(200).send({
          success: true,
          message: "Push notification status update successfully.",
        });
      } else {
        reply.status(422).send({
          success: false,
          error: "Unprocessable Entity",
          message: "Please enter correct status code.",
        });
      }
    } else {
      reply.status(401).send({
        success: false,
        error: "Unauthorized",
        message: "Authorization token is missing or invalid.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Unauthorized",
      message: "Authorization token is missing or invalid.",
    });
  }
};

const ratePass = async (req, reply) => {
  try {
    // Decode token
    const authContact = await decoder.contact(req.headers.authorization);

    if (authContact !== "") {
      // Get user d

      const connection = await global.fastify.mysql.getConnection();

      const [rows] = await connection.query(
        "SELECT `id` FROM `users` where `contact` = ?",
        [authContact]
      );

      if (rows.length > 0) {
        const userId = rows[0].id;

        const { pass_id, rate } = req.body;

        // check if exists
        const [exists] = await connection.query(
          "SELECT `rate` FROM `pass_ratings` WHERE `user_id` = ? AND `pass_id` = ?",
          [userId, pass_id]
        );
        if (exists.length > 0) {
          // update here
          await connection.query(
            "UPDATE `pass_ratings` SET `rate` = ? WHERE `user_id` = ? AND `pass_id` = ?",
            [rate, userId, pass_id]
          );
        } else {
          // Insert here
          await connection.query(
            "INSERT INTO `pass_ratings`(`user_id`, `pass_id`, `rate`) VALUES (?, ?, ?)",
            [userId, pass_id, rate]
          );
        }

        connection.release();

        reply.status(200).send({
          success: true,
          message: "Pass rated successfully.",
        });
      } else {
        connection.release();
        reply.status(401).send({
          success: false,
          error: "Unauthorized",
          message: "Authorization token is invalid.",
        });
      }
    } else {
      reply.status(401).send({
        success: false,
        error: "Unauthorized",
        message: "Authorization token is missing or invalid.",
      });
    }
  } catch (error) {
    reply.status(401).send({
      success: false,
      error: "Unauthorized",
      message: "Authorization token is missing or invalid.",
    });
  }
};

const updateFcmToken = async (req, reply) => {
  try {
    // Decode token
    const authContact = await decoder.contact(req.headers.authorization);

    if (authContact !== "") {
      // Get user id

      const connection = await global.fastify.mysql.getConnection();

      const [rows] = await connection.query(
        "SELECT `id` FROM `users` where `contact` = ?",
        [authContact]
      );

      if (rows.length > 0) {
        const userId = rows[0].id;
        const { fcmToken } = req.body;

        // check if exists
        const [exists] = await connection.query(
          "SELECT `id` FROM `user_fcm_tokens` WHERE `user_id` = ? AND `token` = ?",
          [userId, fcmToken]
        );
        if (exists.length < 1) {
          // Insert here
          await connection.query(
            "INSERT INTO `user_fcm_tokens`(`user_id`, `token`) VALUES (?, ?)",
            [userId, fcmToken]
          );
        }

        connection.release();

        reply.status(200).send({
          success: true,
          message: "FCM Token added successfully.",
          data: {},
        });
      } else {
        connection.release();
        reply.status(401).send({
          success: false,
          error: "Unauthorized",
          message: "Authorization token is invalid.",
        });
      }
    } else {
      reply.status(401).send({
        success: false,
        error: "Unauthorized",
        message: "Authorization token is missing or invalid.",
      });
    }
  } catch (error) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong. Unable to save FCM token",
      data: {
        err,
      },
    });
  }
};

const removeFcmToken = async (req, reply) => {
  try {
    // Decode token
    const authContact = await decoder.contact(req.headers.authorization);

    if (authContact !== "") {
      // Get user id

      const connection = await global.fastify.mysql.getConnection();

      const [rows] = await connection.query(
        "SELECT `id` FROM `users` where `contact` = ?",
        [authContact]
      );

      if (rows.length > 0) {
        const { fcmToken } = req.body;

        // check if exists
        const [exists] = await connection.query(
          "SELECT `id` FROM `user_fcm_tokens` WHERE `token` = ?",
          [fcmToken]
        );
        if (exists.length > 0) {
          // Remove from here
          console.log("DELETE FROM `user_fcm_tokens` WHERE");
          await connection.query(
            "DELETE FROM `user_fcm_tokens` WHERE `token` = ?",
            [fcmToken]
          );
        }

        connection.release();

        reply.status(200).send({
          success: true,
          message: "FCM Token removed successfully.",
          data: {},
        });
      } else {
        connection.release();
        reply.status(401).send({
          success: false,
          error: "Unauthorized",
          message: "Authorization token is invalid.",
        });
      }
    } else {
      reply.status(401).send({
        success: false,
        error: "Unauthorized",
        message: "Authorization token is missing or invalid.",
      });
    }
  } catch (error) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong. Unable to save FCM token",
      data: {
        error,
      },
    });
  }
};

const checkIfPaidViaAdmin = async (req, reply) => {
  try {
    // Decode token
    const authContact = await decoder.contact(req.headers.authorization);

    const connection = await global.fastify.mysql.getConnection();

    const [rows] = await connection.query(
      "SELECT `id` FROM `paid_user_via_admin` where `contact` = ?",
      [authContact]
    );

    connection.release();

    reply.status(200).send({
      success: true,
      message: "User status fetched.",
      data: {
        is_paid: rows.length,
      },
    });
  } catch (error) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong. Unable to save FCM token",
      data: {
        error,
      },
    });
  }
};

const updatePaidStatus = async (req, reply) => {
  try {
    // Decode token
    const authContact = await decoder.contact(req.headers.authorization);

    const { status } = req.body;

    const connection = await global.fastify.mysql.getConnection();

    const [rows] = await connection.query(
      "SELECT `id` FROM `users` where `contact` = ?",
      [authContact]
    );

    if (rows.length > 0) {
      id = rows[0].id;
      await connection.query(
        "UPDATE `users` SET `is_hop_plus` = ? WHERE `users`.`id` = ?",
        [status, id]
      );
    }

    connection.release();

    reply.status(200).send({
      success: true,
      message: "User HOP Plus status updated successfully.",
      data: {
        is_paid: status,
      },
    });
  } catch (error) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong. Unable to save paid status",
      data: {
        error,
      },
    });
  }
};

module.exports = {
  profile,
  getRedeemedPasses,
  redeemPass,
  updatePushNotification,
  ratePass,
  updateFcmToken,
  removeFcmToken,
  checkIfPaidViaAdmin,
  updatePaidStatus,
};
