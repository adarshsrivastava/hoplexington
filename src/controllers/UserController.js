const otpHelper = require("../helper/OtpGenerator");
const smsHelper = require("../helper/SmsHelper");

const checkUserExists = async (req, reply) => {
  try {
    const contact = req.params.contact;
    if (contact !== "") {
      // DB Stuffs
      const connection = await global.fastify.mysql.getConnection();
      const [rows] = await connection.query(
        "SELECT contact FROM `users` where `contact` = ?",
        [contact]
      );
      connection.release();

      if (rows.length > 0) {
        // update user OTP

        // Generate random OTP
        const otp = await otpHelper.generateOtp();

        // send OTP to user mobile
        const msg = "Your verification code : " + otp + ". Thank you.";
        const sendSuccess = await smsHelper.send(contact, msg);

        if (sendSuccess === true) {
          // update IN DB
          const connection = await global.fastify.mysql.getConnection();
          await connection.query(
            "UPDATE `users` SET `otp` = ? where `contact` = ?",
            [otp, contact]
          );
          connection.release();

          reply.status(200).send({
            success: true,
            message: "OTP send successfully.",
            data: {
              isExists: 1,
            },
          });
        } else {
          reply.status(422).send({
            success: false,
            message: "Oops! Unable to send OTP.",
            data: {
              isExists: 1,
            },
          });
        }
      } else {
        // Create newUserOtp

        // Generate random OTP
        const otp = await otpHelper.generateOtp();

        // send OTP to user mobile
        const msg = "Your verification code : " + otp + ". Thank you";
        const sendSuccess = await smsHelper.send(contact, msg);

        if (sendSuccess === true) {
          // update IN DB
          const connection = await global.fastify.mysql.getConnection();
          // check new user is saved
          const [rows] = await connection.query(
            "SELECT `contact` FROM `new_user_otp` WHERE `contact` = ?",
            [contact]
          );

          if (rows.length > 0) {
            // Update new OTP
            const otpStatus = 0;
            await connection.query(
              "UPDATE `new_user_otp` SET `otp` = ?, `otp_verified` = ? where `contact` = ?",
              [otp, otpStatus, contact]
            );
          } else {
            // Create new sign up user
            await connection.query(
              "INSERT INTO `new_user_otp`(`contact`, `otp`) VALUES (?, ?)",
              [contact, otp]
            );
          }
          connection.release();

          reply.status(200).send({
            success: true,
            message: "OTP send successfully.",
            data: { isExists: 0 },
          });
        } else {
          reply.status(422).send({
            success: false,
            message: "Oops! Unable to send OTP.",
            data: { isExists: 0 },
          });
        }
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Contact field is required.",
        data: {},
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {
        err,
      },
    });
  }
};

const userLogin = async (req, reply) => {
  try {
    //console.log(req.body)
    const { contact, otp, deviceID } = req.body;

    if (contact !== "" && otp !== "") {
      // DB Operation
      const connection = await global.fastify.mysql.getConnection();
      const [rows] = await connection.query(
        "SELECT `id`,`first_name`,`profile_image`,`contact`,`dob`,`is_hop_plus`,`status`,`location_id`,`device_ids` FROM `users` WHERE `contact` = ? AND `otp` = ?",
        [contact, otp]
      );

      if (rows.length > 0) {
        let ids = rows[0].device_ids;

        // verify or append device ids
        if (rows[0].device_ids !== "" && rows[0].device_ids !== null) {
          ids = JSON.parse(rows[0].device_ids);
          // if not exists
          if (ids.indexOf(deviceID) < 0) {
            // Append new device ID
            ids.push(deviceID);
          }
          ids = JSON.stringify(ids);
        } else {
          const devID = new Array();
          devID.push(deviceID);
          ids = JSON.stringify(devID);
        }

        // Update device IDs
        await connection.query(
          "UPDATE `users` SET `device_ids` = ? WHERE `contact` = ? AND `otp` = ?",
          [ids, contact, otp]
        );

        let user_payload = rows[0];
        const [location] = await connection.query(
          "SELECT * FROM `location` WHERE `id` = ?",
          [user_payload.location_id]
        );
        user_payload.location = location[0] || {};

        //console.log(user_payload);

        const token = global.fastify.jwt.sign({
          contact,
          payload: user_payload,
        });

        connection.release();
        reply.status(200).send({
          success: true,
          message: "Login success.",
          data: {
            token,
            user: user_payload,
          },
        });
      } else {
        connection.release();
        reply.status(422).send({
          success: false,
          message: "Oops! OTP not matched.",
          data: {},
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Contact and OTP fields are required.",
        data: {},
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {},
    });
  }
};

const userRegister = async (req, reply) => {
  try {
    const { contact, otp, firstName, dob, deviceID, referral } = req.body;

    if (contact !== "" && otp !== "") {
      // Check user OTP
      const otpVerified = 1;
      const connection = await global.fastify.mysql.getConnection();
      const [rows] = await connection.query(
        "SELECT contact FROM `new_user_otp` where `contact` = ? AND `otp_verified` = ?",
        [contact, otpVerified]
      );

      if (rows.length > 0) {
        // Device ID
        const devID = new Array();
        devID.push(deviceID);
        const deviID = JSON.stringify(devID);
        // Create new user
        await connection.query(
          "INSERT INTO `users`(`first_name`, `contact`, `dob`, `device_ids`, `referred_by`) VALUES (?, ?, ?, ?, ?)",
          [firstName, contact, dob, deviID, referral]
        );

        // Remove otp user
        await connection.query(
          "DELETE FROM `new_user_otp` WHERE `contact` = ?",
          [contact]
        );

        const [new_user] = await connection.query(
          "SELECT `id`,`first_name`,`profile_image`,`contact`,`dob`,`is_hop_plus`,`status`,`location_id`,`device_ids` FROM `users` WHERE `contact` = ?",
          [contact]
        );

        let user_payload = new_user[0];
        const [location] = await connection.query(
          "SELECT * FROM `location` WHERE `id` = ?",
          [user_payload.location_id]
        );
        user_payload.location = location[0] || {};

        //console.log(user_payload);

        const token = global.fastify.jwt.sign({
          contact,
          payload: user_payload,
        });
        connection.release();

        // generate token

        reply.status(200).send({
          success: true,
          message: "Registration success.",
          data: {
            token,
            user: user_payload,
          },
        });
      } else {
        connection.release();
        reply.status(422).send({
          success: false,
          message: "Your contact is not verified yet.",
          data: {},
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Mandatory fields are required.",
        data: {},
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {},
    });
  }
};

const verifyNewUserOtp = async (req, reply) => {
  try {
    const { contact, otp } = req.body;
    //console.log(contact, otp);
    if (contact !== "" && otp !== "") {
      // update IN DB
      const connection = await global.fastify.mysql.getConnection();
      // check new user is saved
      const [rows] = await connection.query(
        "SELECT `contact` FROM `new_user_otp` WHERE `contact` = ? AND otp = ?",
        [contact, otp]
      );

      if (rows.length > 0) {
        // Update new OTP
        const otpStatus = 1;
        await connection.query(
          "UPDATE `new_user_otp` SET `otp_verified` = ? where `contact` = ? AND `otp` = ?",
          [otpStatus, contact, otp]
        );

        connection.release();

        reply.status(200).send({
          success: true,
          message: "Contact verified successfully.",
          data: {},
        });
      } else {
        connection.release();
        // OTP not matched
        reply.status(422).send({
          success: false,
          message: "Sorry! OTP not matched.",
          data: {},
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Contact and OTP fields are required.",
        data: {},
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {
        err,
      },
    });
  }
};

const verifyReferral = async (req, reply) => {
  try {
    const { referral } = req.body;

    if (referral !== "" && referral !== null) {
      // update IN DB
      const connection = await global.fastify.mysql.getConnection();
      // check new user is saved
      const [rows] = await connection.query(
        "SELECT `id` FROM `paid_user_via_admin` WHERE `referral` = BINARY ?",
        [referral]
      );

      const [isExistsInMore] = await connection.query(
        "SELECT `id` FROM `ambassador_referrals` WHERE `referral` = BINARY ?",
        [referral]
      );

      if (rows.length > 0 || isExistsInMore.length > 0) {
        connection.release();

        reply.status(200).send({
          success: true,
          message: "Entered Referral code is valid.",
          data: {
            isExists: true,
            referral,
          },
        });
      } else {
        connection.release();
        // OTP not matched
        reply.status(422).send({
          success: false,
          message: "Sorry! Referral code does not exists.",
          data: {
            isExists: false,
            referral,
          },
        });
      }
    } else {
      reply.status(422).send({
        success: false,
        message: "Please enter referral code.",
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {
        err,
      },
    });
  }
};

const rmUser = async (req, reply) => {
  try {
    const contact = req.params.contact;
    if (contact !== "") {
      // DB Stuffs
      const connection = await global.fastify.mysql.getConnection();
      const isRemoved = await connection.query(
        "DELETE FROM `users` WHERE `contact` = ?",
        [contact]
      );
      connection.release();
      //console.log("User Removed", isRemoved);
      reply.status(200).send({
        success: true,
        message: "User removed successfully.",
        data: {},
      });
    } else {
      reply.status(422).send({
        success: false,
        message: "Contact field is required.",
        data: {},
      });
    }
  } catch (err) {
    reply.status(422).send({
      success: false,
      message: "Oops! Something went wrong.",
      data: {
        err,
      },
    });
  }
};

module.exports = {
  checkUserExists,
  userLogin,
  userRegister,
  verifyNewUserOtp,
  verifyReferral,
  rmUser,
};
