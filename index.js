const fastify = require("fastify")({ logger: true });

// Plugins registered
fastify.register(require("fastify-axios"));
fastify.register(require("./src/plugins/env"));
fastify.register(require("./src/plugins/mySql"));
fastify.register(require("./src/plugins/swagger"));
fastify.register(require("./src/plugins/jwt-token"));
// Admin middleware for role authentication
fastify.register(require("./src/middleware/admin/isAdmin"));

// Import Push notification controller
const PushMessagingController = require("./src/controllers/messaging/PushMessagingController");

// Firebase
const { admin } = require("./src/services/pushNotification");
const notificationOptions = {
  priority: "high",
  timeToLive: 60 * 60 * 24,
};

// Fire base END

const path = require("path");

fastify.register(require("fastify-static"), {
  root: path.join(__dirname, "media"),
  prefix: "/view-file/", // optional: default '/'
});

// Add multer for media upload on server
const multer = require("fastify-multer"); // or import multer from 'fastify-multer'
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "media/");
  },
  filename: function (req, file, cb) {
    const fileExt = file.originalname.split(".").pop();
    cb(null, Date.now() + "." + fileExt);
  },
});

const upload = multer({ storage: storage });
fastify.register(multer.contentParser);

// Routes
fastify.register(require("./src/routes/user/userRoutes"), {
  prefix: "api/user",
});
// User Dashboard routes
fastify.register(require("./src/routes/user/dashboard/userDashboard"), {
  prefix: "api/user-dashboard",
});
// Passes routes
fastify.register(require("./src/routes/passes/passesRoute"), {
  prefix: "api/passes",
});
// Admin routes authentication
fastify.register(require("./src/routes/admin/authRoute"), {
  prefix: "api/admin-auth",
});
// Admin routes for passes operation
fastify.register(require("./src/routes/admin/passesRoutes"), {
  prefix: "api/admin",
});
// Admin routes for users operation
fastify.register(require("./src/routes/admin/userRoute"), {
  prefix: "api/admin",
});

//Admin Routes for Location Related Operations
fastify.register(require("./src/routes/admin/locationRoutes"), {
  prefix: "api/admin",
});

fastify.register(require("./src/routes/location/locationRoute"), {
  prefix: "api/v2",
});

fastify.register(require("./src/routes/bar/barRoute"), {
  prefix: "api/v2",
});

// analytics

fastify.register(require("./src/routes/analytics/analyticsRoute"), {
  prefix: "api/admin",
});

// fastify.register(require("./src/routes/bar/barRoute"), {
//   prefix: "api/bar",
// });

fastify.get("/", async (request, reply) => {
  return { success: "Response Success." };
});

// or using the short hand declaration
const AdminController = require("./src/controllers/admin/AdminController");
const schemas = require("./src/schema/admin/adminSchema");

fastify.post(
  "/api/admin/add-media",
  { schema: schemas.addMedia, preHandler: upload.single("file") },
  AdminController.addMedia
);

fastify.sendPushNotification = async (
  title,
  body,
  fcmToken,
  currentStatus,
  passId = ""
) => {
  if (passId != "" && passId != null) {
    passId = passId.toString();
  } else {
    passId = "";
  }
  const registrationToken = fcmToken;
  let data = {
    sender: "HOP",
    status: currentStatus,
    pass_id: passId,
  };

  const message = {
    notification: {
      title,
      body,
    },
    data,
  };
  const options = notificationOptions;
  await admin
    .messaging()
    .sendToDevice(registrationToken, message, options)
    .then((response) => {
      console.log("Notification sent successfully: ", response);
    })
    .catch((error) => {
      console.log("Notification send Error: ", error);
    });

  return {
    success: true,
    message: "Notification sent successfully.",
  };
};

// Push Notification CRONs
fastify.register(require("fastify-cron"), {
  jobs: [
    {
      cronTime: "0 * * * * *",
      onTick: async (server) => {
        // Check and push notification pass is active now
        await PushMessagingController.sendNotification();
      },
    },
  ],
});

const PORT = process.env.PORT || 5000;
const HOST = process.env.HOST || "127.0.0.1";

// const HOST = process.env.HOST || "96.126.113.216";
const stripe = require("stripe")(process.env.STRIPEKEY);

const start = async () => {
  try {
    await fastify.listen(PORT, HOST, (err, address) => {
      if (err) {
        console.log(err);
        process.exit(1);
      }
      // fastify.cron.startAllJobs();
      // Global variable assign
      global.fastify = fastify;
      global.upload = upload;
      global.stripe = stripe;

      console.log("address - ", address);
    });
  } catch (err) {
    fastify.log.error(err);
    console.log(err);
    process.exit(1);
  }
};
start();
